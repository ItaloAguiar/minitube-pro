﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using YouTube.API.DataModel.Subscriptions;
using YouTube.API.OAuth2;
using YouTube.API.Parameters;
using System.Runtime.Serialization.Json;

namespace YouTube.API
{
    public static class Subscriptions
    {
        private const string requestUri = "https://www.googleapis.com/youtube/v3/subscriptions";

        public async static Task<Item> Insert(AccessToken token, string ChannelId)
        {
            string body = "{\"snippet\":{\"resourceId\":{\"channelId\": \"" + ChannelId + "\"}}}";
            return await InternetHelper.PostAsync<Item>(token, string.Format("{0}?part=snippet&key={1}", requestUri, Key.GetPublicKey()),body);            
        }
        public async static Task<SubscriptionsResponse> List(SubscriptionsParameters parameters)
        {
            return await InternetHelper.GetAsync<SubscriptionsResponse>(requestUri, parameters);            
        }
        public async static Task<SubscriptionsResponse> List(AccessToken token, SubscriptionsParameters parameters)
        {
            return await InternetHelper.GetAsync<SubscriptionsResponse>(token, requestUri, parameters);
        }
        public async static Task Delete(AccessToken token, string channelId)
        {
            try
            {
                string url = string.Format("https://www.googleapis.com/youtube/v3/subscriptions?id={0}&key={1}", channelId, Key.GetPublicKey());

                HttpClient http = new System.Net.Http.HttpClient();
                HttpRequestMessage msg = new HttpRequestMessage(HttpMethod.Delete, url);
                msg.Headers.Add("Authorization", token.token_type + " " + token.access_token);

                HttpResponseMessage response = await http.SendAsync(msg);
                if (!(response.StatusCode == System.Net.HttpStatusCode.OK || response.StatusCode == System.Net.HttpStatusCode.NoContent))
                {
                    string r = await response.Content.ReadAsStringAsync();
                    System.Diagnostics.Debug.WriteLine(r);
                    throw new ApiException(r);
                }
            }
            catch (Exception e)
            {
                throw new ApiException(e.Message);
            }
        }
    }

}
