﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using YouTube.API.DataModel.CommentThreads;
using YouTube.API.Parameters;
using System.Runtime.Serialization.Json;
using YouTube.API.OAuth2;

namespace YouTube.API
{
    public static class CommentThreads
    {
        private const string requestUri = "https://www.googleapis.com/youtube/v3/commentThreads";

        public async static Task<Item> Insert(OAuth2.AccessToken token, string comment, string videoId)
        {
            string body = "{\"snippet\": {\"topLevelComment\": {\"snippet\":{\"videoId\":\"" + videoId +
                   "\",\"textOriginal\": \"" + System.Web.HttpUtility.JavaScriptStringEncode(comment) + "\"}}}}";
            string url = string.Format("{0}?part=snippet", requestUri);

            return await InternetHelper.PostAsync<Item>(token, url, body);
        }
        public static async Task<CommentThreadsResponse> List(CommentThreadsParameters parameters)
        {
            return await InternetHelper.GetAsync<CommentThreadsResponse>(requestUri, parameters);            
        }

        public static async Task<CommentThreadsResponse> List(AccessToken token, CommentThreadsParameters parameters)
        {
            return await InternetHelper.GetAsync<CommentThreadsResponse>(token, requestUri, parameters);
        }

        public static void Update()
        {

        }
    }
}
