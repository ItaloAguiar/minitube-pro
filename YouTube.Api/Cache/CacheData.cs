﻿using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTube.API.Cache
{
    internal class CacheData
    {
        static CacheData()
        {
            InitializeDatabase();
        }

        public static void InitializeDatabase()
        {
            using (SqliteConnection db =
                new SqliteConnection("Filename=cacheindex.db"))
            {
                db.Open();

                String tableCommand = @"create table if not exists webindex(
                  url text primary key,
                  request_date integer,
                  request_content text
                ); ";

                SqliteCommand createTable = new SqliteCommand(tableCommand, db);

                createTable.ExecuteReader();
            }
        }

        public static string RequestData(string url, int cache_time = 10)
        {
            string r = null;
            using (SqliteConnection db =
        new SqliteConnection("Filename=cacheindex.db"))
            {
                db.Open();

                string t = (DateTime.Now.Subtract(new TimeSpan(0,cache_time,0))).ToString("yyyyMMddhhmmss");
                long time = long.Parse(t);

                string command = "SELECT request_content from webindex where url = @url and request_date >= @time";

                if(cache_time == 0)
                {
                    command = "SELECT request_content from webindex where url = @url";
                }

                SqliteCommand selectCommand = new SqliteCommand(command, db);

                selectCommand.Parameters.AddWithValue("@url", url);
                selectCommand.Parameters.AddWithValue("@time", time);

                SqliteDataReader query = selectCommand.ExecuteReader();

                while (query.Read())
                {
                    r = query.GetString(0);
                }

                db.Close();
            }

            return r;
        }
        public static void AddData(string url, string content)
        {

            using (SqliteConnection db =
        new SqliteConnection("Filename=cacheindex.db"))
            {
                db.Open();

                SqliteCommand insertCommand = new SqliteCommand();
                insertCommand.Connection = db;

                string t = DateTime.Now.ToString("yyyyMMddhhmmss");
                long time = long.Parse(t);

                // Use parameterized query to prevent SQL injection attacks
                insertCommand.CommandText = "INSERT OR IGNORE INTO webindex(url, request_date, request_content) VALUES (@url, @date,@content); " +
                    "UPDATE webindex SET request_date = @date, request_content = @content where url = @url";
                insertCommand.Parameters.AddWithValue("@url", url);
                insertCommand.Parameters.AddWithValue("@date", time);
                insertCommand.Parameters.AddWithValue("@content", content);

                insertCommand.ExecuteReader();

                db.Close();
            }
        }
    }
}
