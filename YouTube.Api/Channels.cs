﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTube.API.DataModel;
using YouTube.API.Parameters;

namespace YouTube.API
{
    public class Channels
    {
        private const string requestUri = "https://www.googleapis.com/youtube/v3/channels";

        public async static Task<ChannelsResponse> List(ChannelsParameter parameters)
        {
            return await InternetHelper.GetAsync<ChannelsResponse>(requestUri, parameters);
        }
        public void Update()
        {

        }
    }
}
