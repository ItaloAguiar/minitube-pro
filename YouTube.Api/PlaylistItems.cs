﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using YouTube.API.DataModel.PlaylistItems;
using System.Runtime.Serialization.Json;

namespace YouTube.API
{
    public static class PlaylistItems
    {
        private const string requestUri = "https://www.googleapis.com/youtube/v3/playlistItems";
        public static void Insert()
        {

        }
        public static async Task<PlaylistItemsResponse> List(Parameters.PlaylistItemsParemeters parameters)
        {
            return await InternetHelper.GetAsync<PlaylistItemsResponse>(requestUri, parameters);            
        }
        public static void Delete()
        {

        }
        public static void Update()
        {

        }
    }

}
