﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using Windows.Data.Json;

namespace YouTube.API.OAuth2
{
    public class Profile
    {
        public async Task<UserInfo> GetUserInfo(AccessToken token)
        {
            HttpClient http = new System.Net.Http.HttpClient();
            Uri url = new Uri(string.Format("https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token={0}", token.access_token));
            HttpResponseMessage response = await http.GetAsync(url);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new ApiException();
            }
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(UserInfo));
            var user = (UserInfo)ser.ReadObject(await response.Content.ReadAsStreamAsync());
            RegisterWithEpicApps(user);
            return user;
        }
        public async Task<string> GetUserId(AccessToken token)
        {            
            HttpClient http = new System.Net.Http.HttpClient();
            HttpRequestMessage msg = new HttpRequestMessage(HttpMethod.Get,
                string.Format("https://www.googleapis.com/youtube/v3/channels?part=id&mine=true&key={0}",Key.GetPublicKey()));

            msg.Headers.Add("Authorization", token.token_type + " " + token.access_token);

            HttpResponseMessage response = await http.SendAsync(msg);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                string r = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine(r);
                throw new ApiException(r);
            }
            JsonObject result = JsonValue.Parse(await response.Content.ReadAsStringAsync()).GetObject();
            return result["items"].GetArray()[0].GetObject()["id"].GetString();
        }
        private async void RegisterWithEpicApps(UserInfo user)
        {
            try
            {
                string url = "http://api.epicapps.com.br/User/InsertGoogleUser";

                HttpClient http = new System.Net.Http.HttpClient();
                HttpRequestMessage msg = new HttpRequestMessage(HttpMethod.Post, url);
                List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
                values.Add(new KeyValuePair<string, string>("name", user.name));
                values.Add(new KeyValuePair<string, string>("email", user.email));
                values.Add(new KeyValuePair<string, string>("gender", user.gender));
                values.Add(new KeyValuePair<string, string>("locale", user.locale));
                values.Add(new KeyValuePair<string, string>("pictureUrl", user.picture));
                values.Add(new KeyValuePair<string, string>("profileUrl", user.link));
                values.Add(new KeyValuePair<string, string>("verified_email", user.verified_email.ToString()));
                values.Add(new KeyValuePair<string, string>("appid", "10001"));
                var content = new FormUrlEncodedContent(values);
                await http.PostAsync(url, content);
            }
            catch
            {

            }
        }
    }

    public class UserInfo
    {
        public string id { get; set; }
        public string name { get; set; }
        public string given_name { get; set; }
        public string family_name { get; set; }
        public string link { get; set; }
        public string picture { get; set; }
        public string gender { get; set; }
        public string locale { get; set; }
        public string email { get; set; }
        public bool verified_email { get; set; }
    }
}
