﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTube.API.DataModel.Comment;

namespace YouTube.API
{
    public static class Comments
    {
        private const string requestUri = "https://www.googleapis.com/youtube/v3/comments";

        public async static Task<CommentResponse> Insert(OAuth2.AccessToken token, string comment, string parentId)
        {
            string c = System.Web.HttpUtility.JavaScriptStringEncode(comment);
            string body = "{\"snippet\":{\"textOriginal\": \"" + c + "\",\"parentId\": \"" + parentId + "\"}}";
            string url = string.Format("{0}?part=snippet", requestUri);

            return await InternetHelper.PostAsync<CommentResponse>(token, url, body);
        }
        public static void List()
        {

        }
        public static void Delete()
        {

        }
        public static void Update()
        {

        }
        public static void MarkAsSpam()
        {

        }
        public static void SetModerationStatus()
        {

        }
    }
}
