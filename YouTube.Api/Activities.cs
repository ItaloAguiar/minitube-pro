﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTube.API.DataModel.Activities;
using YouTube.API.DataModel.Videos;
using YouTube.API.OAuth2;
using YouTube.API.Parameters;

namespace YouTube.API
{
    public class Activities
    {
        private const string requestUri = "https://www.googleapis.com/youtube/v3/activities";

        public void Insert()
        {

        }

        public async Task<ActivitiesResponse> List(ActivitiesParameters p)
        {
            return await InternetHelper.GetAsync<ActivitiesResponse>(requestUri, p);
        }

        public async Task<ActivitiesResponse> List(AccessToken token, ActivitiesParameters p)
        {
            return await InternetHelper.GetAsync<ActivitiesResponse>(token, requestUri, p);
        }
        public void Delete()
        {

        }
    }    
}
