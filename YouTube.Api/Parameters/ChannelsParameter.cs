﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace YouTube.API.Parameters
{
    public class ChannelsParameter:Parameter
    {
        /// <summary>
        /// The part parameter specifies a comma-separated list of one or 
        /// more channel resource properties that the API response will 
        /// include. If the parameter identifies a property that contains 
        /// child properties, the child properties will be included in the 
        /// response. For example, in a channel resource, the contentDetails 
        /// property contains other properties, such as the uploads properties. 
        /// As such, if you set part=contentDetails, the API response will also 
        /// contain all of those nested properties. (string)
        /// </summary>
        public string part { get; set; } = "snippet";


        /// <summary>
        /// The categoryId parameter specifies a YouTube guide category, thereby 
        /// requesting YouTube channels associated with that category.
        /// </summary>
        public string categoryId { get; set; }

        /// <summary>
        /// The forUsername parameter specifies a YouTube username, thereby 
        /// requesting the channel associated with that username. 
        /// </summary>
        public string forUsername { get; set; }

        /// <summary>
        /// The hl parameter should be used for filter out the properties 
        /// that are not in the given language. Used for the brandingSettings part.
        /// </summary>
        public string hl { get; set; }

        /// <summary>
        /// The id parameter specifies a comma-separated list of the YouTube 
        /// channel ID(s) for the resource(s) that are being retrieved. In a 
        /// channel resource, the id property specifies the channel's YouTube 
        /// channel ID. 
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// Note: This parameter is intended exclusively for YouTube content 
        /// partners. Set this parameter's value to true to instruct the API 
        /// to only return channels managed by the content owner that the 
        /// onBehalfOfContentOwner parameter specifies. The user must be 
        /// authenticated as a CMS account linked to the specified content 
        /// owner and onBehalfOfContentOwner must be provided.
        /// </summary>
        public bool? managedByMe { get; set; }

        /// <summary>
        /// The maxResults parameter specifies the maximum number of items 
        /// that should be returned in the result set.
        /// </summary>
        public int maxResults { get; set; } = 50;

        /// <summary>
        /// Set this parameter's value to true to instruct the API to only 
        /// return channels owned by the authenticated user.
        /// </summary>
        public bool? mine { get; set; }

        /// <summary>
        /// Use the subscriptions.list method and its mySubscribers 
        /// parameter to retrieve a list of subscribers to the 
        /// authenticated user's channel.
        /// </summary>
        public bool? mySubscribers { get; set; }

        /// <summary>
        /// Note: This parameter is intended exclusively for YouTube 
        /// content partners. The onBehalfOfContentOwner parameter 
        /// indicates that the request's authorization credentials 
        /// identify a YouTube CMS user who is acting on behalf of 
        /// the content owner specified in the parameter value. This 
        /// parameter is intended for YouTube content partners that 
        /// own and manage many different YouTube channels. It allows 
        /// content owners to authenticate once and get access to all 
        /// their video and channel data, without having to provide 
        /// authentication credentials for each individual channel. 
        /// The CMS account that the user authenticates with must be 
        /// linked to the specified YouTube content owner.
        /// </summary>
        public string onBehalfOfContentOwner { get; set; }

        /// <summary>
        /// Selector specifying which fields to include in a partial 
        /// response.
        /// </summary>
        public string fields { get; set; }


        public override string ToString()
        {
            string c = "?";
            foreach (PropertyInfo p in this.GetType().GetRuntimeProperties())
            {
                if (p.GetValue(this, null) != null)
                {
                    c += string.Format("{0}={1}&", p.Name, p.GetValue(this, null));
                }
            }
            return c;
        }
    }
}
