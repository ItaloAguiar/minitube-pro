﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace YouTube.API.Parameters
{
    public class ActivitiesParameters:Parameter
    {
        /// <summary>
        /// The part parameter specifies a comma-separated list 
        /// of one or more activity resource properties that the 
        /// API response will include. If the parameter identifies 
        /// a property that contains child properties, the child 
        /// properties will be included in the response. For example, 
        /// in an activity resource, the snippet property contains 
        /// other properties that identify the type of activity, a 
        /// display title for the activity, and so forth. If you set 
        /// part=snippet, the API response will also contain all 
        /// of those nested properties.
        /// </summary>
        public string part { get; set; } = "snippet";

        /// <summary>
        /// The channelId parameter specifies a unique YouTube channel 
        /// ID. The API will then return a list of that channel's activities.
        /// </summary>
        public string channelId { get; set; }

        /// <summary>
        /// Set this parameter's value to true to retrieve the activity feed 
        /// that displays on the YouTube home page for the currently authenticated user.
        /// </summary>
        public bool? home { get; set; }

        /// <summary>
        /// Set this parameter's value to true to retrieve a feed of 
        /// the authenticated user's activities.
        /// </summary>
        public bool? mine { get; set; }


        /// <summary>
        /// The publishedAfter parameter specifies the earliest date and 
        /// time that an activity could have occurred for that activity 
        /// to be included in the API response. If the parameter value 
        /// specifies a day, but not a time, then any activities that 
        /// occurred that day will be included in the result set. The 
        /// value is specified in ISO 8601 (YYYY-MM-DDThh:mm:ss.sZ) format.
        /// </summary>
        public string publishedAfter { get; set; }


        /// <summary>
        /// The publishedBefore parameter specifies the date and time 
        /// before which an activity must have occurred for that activity 
        /// to be included in the API response. If the parameter value 
        /// specifies a day, but not a time, then any activities that 
        /// occurred that day will be excluded from the result set. The 
        /// value is specified in ISO 8601 (YYYY-MM-DDThh:mm:ss.sZ) format.
        /// </summary>
        public string publishedBefore { get; set; }

        /// <summary>
        /// The regionCode parameter instructs the API to return results 
        /// for the specified country. The parameter value is an ISO 3166-1 
        /// alpha-2 country code. YouTube uses this value when the authorized 
        /// user's previous activity on YouTube does not provide enough 
        /// information to generate the activity feed. 
        /// </summary>
        public string regionCode { get; set; }


        /// <summary>
        /// Selector specifying which fields to include in a partial response.
        /// </summary>
        public string fields { get; set; }

        /// <summary>
        /// The maxResults parameter specifies the maximum number of items that should 
        /// be returned in the result set. Acceptable values are 0 to 50, inclusive. 
        /// The default value is 25.
        /// </summary>
        public uint maxResults { get; set; } = 25;


        public override string ToString()
        {
            string c = "?";
            foreach (PropertyInfo p in this.GetType().GetRuntimeProperties())
            {
                if (p.GetValue(this, null) != null)
                {
                    c += string.Format("{0}={1}&", p.Name, p.GetValue(this, null));
                }
            }
            return c;
        }
    }
}
