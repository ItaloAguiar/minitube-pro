﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using YouTube.API.DataModel.Comment;

namespace YouTube.API.DataModel.CommentThreads
{
    [DataContract]
    public class CommentThreadsResponse : Response
    {
        [DataMember(Name ="items")]
        public Item[] Items { get; set; }

        public override CommonItem[] GetItems()
        {
            return Items;
        }
    }

    [DataContract]
    public class Item:CommonItem
    {
        [DataMember(Name ="id")]
        public string Id { get; set; }

        [DataMember(Name = "snippet")]
        public Snippet Snippet { get; set; }

        [DataMember(Name = "replies")]
        public Replies Replies { get; set; }
    }

    [DataContract]
    public class Snippet
    {
        [DataMember(Name = "channelId ")]
        public string ChannelId { get; set; }

        [DataMember(Name = "videoId")]
        public string VideoId { get; set; }

        [DataMember(Name = "topLevelComment")]
        public Toplevelcomment TopLevelComment { get; set; }

        [DataMember(Name = "canReply")]
        public bool CanReply { get; set; }

        [DataMember(Name = "totalReplyCount ")]
        public int TotalReplyCount { get; set; }

        [DataMember(Name = "isPublic")]
        public bool IsPublic { get; set; }
    }

    [DataContract]
    public class Toplevelcomment:CommonItem
    {
        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "snippet")]
        public CommentSnippet Snippet { get; set; }
    }

    [DataContract]
    public class Replies
    {
        [DataMember(Name = "comments")]
        public ObservableCollection<CommentResponse> Comments { get; set; }
    }
}
