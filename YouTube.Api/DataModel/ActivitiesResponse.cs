﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace YouTube.API.DataModel.Activities
{
    [DataContract]
    public class ActivitiesResponse : Response
    {
        public override CommonItem[] GetItems()
        {
            return Items.ToArray();
        }

        [DataMember(Name = "items")]
        public List<Item> Items { get; set; }
    }


    [DataContract]
    public class Snippet:CommonSnippet
    {
        [DataMember(Name = "type")]
        public string Type { get; set; }
    }

    [DataContract]
    public class Upload
    {
        [DataMember(Name = "videoId")]
        public string VideoId { get; set; }
    }

    [DataContract]
    public class ContentDetails
    {
        [DataMember(Name = "upload")]
        public Upload Upload { get; set; }
    }

    [DataContract]
    public class Item:CommonItem
    {
        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "snippet")]
        public Snippet Snippet { get; set; }

        [DataMember(Name = "contentDetails")]
        public ContentDetails ContentDetails { get; set; }
    }
}
