﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTube.API.DataModel.PlaylistItems
{

    public class PlaylistItemsResponse:Response
    {
        public Item[] items { get; set; }

        public override CommonItem[] GetItems()
        {
            return items;
        }
    }

    public class Pageinfo
    {
        public int totalResults { get; set; }
        public int resultsPerPage { get; set; }
    }

    public class Item:CommonItem
    {
        public string id { get; set; }
        public Snippet snippet { get; set; }
    }
    public class Snippet:CommonSnippet
    {        
        public string playlistId { get; set; }
        public int position { get; set; }
        public Resourceid resourceId { get; set; }
    }    

    
    public class Resourceid
    {
        public string kind { get; set; }
        public string videoId { get; set; }
    }

}
