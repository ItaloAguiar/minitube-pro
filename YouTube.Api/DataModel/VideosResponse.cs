﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace YouTube.API.DataModel.Videos
{
    [DataContract]
    public class VideosResponse : Response
    {
        [DataMember(Name = "items")]
        public Item[] Items { get; set; }

        public override CommonItem[] GetItems()
        {
            return Items;
        }
    }

    [DataContract]
    public class Item:CommonItem
    {
        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "snippet")]
        public Snippet Snippet { get; set; }

        [DataMember(Name = "contentDetails")]
        public Contentdetails ContentDetails { get; set; }

        [DataMember(Name = "status")]
        public Status Status { get; set; }

        [DataMember(Name = "statistics")]
        public Statistics Statistics { get; set; }

        [DataMember(Name = "player")]
        public Player Player { get; set; }

        [DataMember(Name = "topicDetails")]
        public Topicdetails TopicDetails { get; set; }
    }

    [DataContract]
    public class Snippet:CommonSnippet
    {
        [DataMember(Name = "categoryId")]
        public string CategoryId { get; set; }

        [DataMember(Name = "liveBroadcastContent")]
        public string LiveBroadcastContent { get; set; }

        [DataMember(Name = "localized")]
        public Localized Localized { get; set; }
    }

    [DataContract]
    public class Localized
    {
        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }
    }

    [DataContract]
    public class Contentdetails
    {
        [DataMember(Name = "duration")]
        public string Duration { get; set; }

        [DataMember(Name = "dimension")]
        public string Dimension { get; set; }

        [DataMember(Name = "definition")]
        public string Definition { get; set; }

        [DataMember(Name = "caption")]
        public string Caption { get; set; }

        [DataMember(Name = "licensedContent")]
        public bool LicensedContent { get; set; }

        [DataMember(Name = "regionRestriction")]
        public Regionrestriction RegionRestriction { get; set; }

        public string FormattedDuration
        {
            get
            {
                try
                {
                    TimeSpan t = XmlConvert.ToTimeSpan(Duration);
                    var time = t.ToString(@"mm\:ss");
                    return time;
                }
                catch
                {
                    return "00:00";
                }
            }
        }
    }

    [DataContract]
    public class Regionrestriction
    {
        [DataMember(Name = "allowed")]
        public string[] Allowed { get; set; }
    }

    [DataContract]
    public class Status
    {
        [DataMember(Name = "uploadStatus")]
        public string UploadStatus { get; set; }

        [DataMember(Name = "privacyStatus")]
        public string PrivacyStatus { get; set; }

        [DataMember(Name = "license")]
        public string License { get; set; }

        [DataMember(Name = "embeddable")]
        public bool Embeddable { get; set; }

        [DataMember(Name = "publicStatsViewable")]
        public bool PublicStatsViewable { get; set; }
    }

    [DataContract]
    public class Statistics
    {
        [DataMember(Name = "viewCount")]
        public long ViewCount { get; set; }

        [DataMember(Name = "likeCount")]
        public long LikeCount { get; set; }

        [DataMember(Name = "dislikeCount")]
        public long DislikeCount { get; set; }

        [DataMember(Name = "favoriteCount")]
        public long FavoriteCount { get; set; }

        [DataMember(Name = "commentCount")]
        public long CommentCount { get; set; }

        public long TotalRatingCount
        {
            get
            {
                return LikeCount + DislikeCount;
            }
        }
    }

    [DataContract]
    public class Player
    {
        [DataMember(Name = "embedHtml")]
        public string EmbedHtml { get; set; }
    }

    [DataContract]
    public class Topicdetails
    {
        [DataMember(Name = "topicIds")]
        public string[] TopicIds { get; set; }

        [DataMember(Name = "relevantTopicIds")]
        public string[] RelevantTopicIds { get; set; }
    }



    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "toplevel", IsNullable = false)]
    public partial class SuggestionResponse
    {

        private toplevelCompleteSuggestion completeSuggestionField;

        /// <remarks/>
        public toplevelCompleteSuggestion CompleteSuggestion
        {
            get
            {
                return this.completeSuggestionField;
            }
            set
            {
                this.completeSuggestionField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class toplevelCompleteSuggestion
    {

        private toplevelCompleteSuggestionSuggestion suggestionField;

        /// <remarks/>
        public toplevelCompleteSuggestionSuggestion suggestion
        {
            get
            {
                return this.suggestionField;
            }
            set
            {
                this.suggestionField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class toplevelCompleteSuggestionSuggestion
    {

        private string dataField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string data
        {
            get
            {
                return this.dataField;
            }
            set
            {
                this.dataField = value;
            }
        }
    }


}
