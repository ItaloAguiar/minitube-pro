﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace YouTube.API.DataModel.Search
{
    public class SearchResponse : Response
    {
        public Item[] items { get; set; }

        public override CommonItem[] GetItems()
        {
            return items;
        }
    }
    [DataContract]
    public class Id
    {
        [DataMember(Name = "kind")]
        public string Kind { get; set; }

        [DataMember(Name = "channelId")]
        public string ChannelId { get; set; }

        [DataMember(Name = "videoId")]
        public string VideoId { get; set; }

        [DataMember(Name = "playlistId")]
        public string PlaylistId { get; set; }
    }

    [DataContract]
    public class Thumbnail
    {
        [DataMember(Name ="url")]
        public string Url { get; set; }
    }

    [DataContract]
    public class Thumbnails
    {
        [DataMember(Name = "default")]
        public Thumbnail Default { get; set; }

        [DataMember(Name = "medium")]
        public Thumbnail Medium { get; set; }

        [DataMember(Name = "high")]
        public Thumbnail High { get; set; }
    }

    [DataContract]
    public class Snippet
    {
        [DataMember(Name = "publishedAt")]
        public string PublishedAt { get; set; }

        [DataMember(Name = "channelId")]
        public string ChannelId { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "thumbnails")]
        public Thumbnails Thumbnails { get; set; }

        [DataMember(Name = "channelTitle")]
        public string ChannelTitle { get; set; }

        [DataMember(Name = "liveBroadcastContent")]
        public string LiveBroadcastContent { get; set; }
    }

    [DataContract]
    public class Item:CommonItem
    {
        [DataMember(Name = "snippet")]
        public Snippet Snippet { get; set; }

        [DataMember(Name = "id")]
        public Id Id { get; set; }
    }



    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "toplevel", IsNullable = false)]
    public partial class SuggestionResponse
    {

        private toplevelCompleteSuggestion[] completeSuggestionField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CompleteSuggestion")]
        public toplevelCompleteSuggestion[] CompleteSuggestion
        {
            get
            {
                return this.completeSuggestionField;
            }
            set
            {
                this.completeSuggestionField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class toplevelCompleteSuggestion
    {

        private toplevelCompleteSuggestionSuggestion suggestionField;

        /// <remarks/>
        public toplevelCompleteSuggestionSuggestion suggestion
        {
            get
            {
                return this.suggestionField;
            }
            set
            {
                this.suggestionField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class toplevelCompleteSuggestionSuggestion
    {

        private string dataField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string data
        {
            get
            {
                return this.dataField;
            }
            set
            {
                this.dataField = value;
            }
        }
    }


}
