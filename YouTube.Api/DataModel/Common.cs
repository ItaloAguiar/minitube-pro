﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace YouTube.API.DataModel
{
    [DataContract]
    public abstract class Response
    {
        [DataMember(Name ="kind")]
        public string Kind { get; set; }

        [DataMember(Name = "etag")]
        public string ETag { get; set; }

        [DataMember(Name = "nextPageToken")]
        public string NextPageToken { get; set; }

        [DataMember(Name = "pageInfo")]
        public PageInfo PageInfo { get; set; }

        public abstract CommonItem[] GetItems();
    }

    [DataContract]
    public class PageInfo
    {
        [DataMember(Name = "totalResults")]
        public int TotalResults { get; set; }

        [DataMember(Name = "resultsPerPage")]
        public int ResultsPerPage { get; set; }
    }

    [DataContract]
    public class CommonItem
    {
        [DataMember(Name = "kind")]
        public string Kind { get; set; }

        [DataMember(Name = "etag")]
        public string ETag { get; set; }        
    }

    [DataContract]
    public class CommonSnippet
    {
        [DataMember(Name = "publishedAt")]
        public string PublishedAt { get; set; }

        [DataMember(Name = "channelId")]
        public string ChannelId { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "thumbnails")]
        public Thumbnails Thumbnails { get; set; }

        [DataMember(Name = "channelTitle")]
        public string ChannelTitle { get; set; }
    }

    [DataContract]
    public class Thumbnails
    {
        [DataMember(Name = "default")]
        public Thumbnail Default { get; set; }

        [DataMember(Name = "medium")]
        public Thumbnail Medium { get; set; }

        [DataMember(Name = "high")]
        public Thumbnail High { get; set; }

        [DataMember(Name = "standard")]
        public Thumbnail Standard { get; set; }

        [DataMember(Name = "maxres")]
        public Thumbnail MaxResolution { get; set; }
    }

    [DataContract]
    public class Thumbnail
    {
        [DataMember(Name = "url")]
        public Uri Url { get; set; }

        [DataMember(Name = "width")]
        public int Width { get; set; }

        [DataMember(Name = "height")]
        public int Height { get; set; }
    }

    [DataContract]
    public class CommentSnippet
    {
        [DataMember(Name = "authorDisplayName")]
        public string AuthorDisplayName { get; set; }

        [DataMember(Name = "authorProfileImageUrl")]
        public Uri AuthorProfileImageUrl { get; set; }

        [DataMember(Name = "authorChannelUrl")]
        public Uri AuthorChannelUrl { get; set; }

        [DataMember(Name = "authorChannelId.value")]
        public string AuthorChannelId { get; set; }

        [DataMember(Name = "textDisplay")]
        public string TextDisplay { get; set; }

        [DataMember(Name = "textOriginal")]
        public string TextOriginal { get; set; }

        [DataMember(Name = "parentId")]
        public string ParentId { get; set; }

        [DataMember(Name = "canRate")]
        public bool CanRate { get; set; }

        [DataMember(Name = "viewerRating")]
        public string ViewerRating { get; set; }

        [DataMember(Name = "likeCount")]
        public int LikeCount { get; set; }

        [DataMember(Name = "publishedAt")]
        public string PublishedAt { get; set; }

        [DataMember(Name = "updatedAt")]
        public string UpdatedAt { get; set; }

        [DataMember(Name = "videoId")]
        public string VideoId { get; set; }
    }
}
