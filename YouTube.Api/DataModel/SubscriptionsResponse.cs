﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTube.API.DataModel.Subscriptions
{
    public class SubscriptionsResponse : Response
    {
        public Item[] items { get; set; }

        public override CommonItem[] GetItems()
        {
            return items;
        }
    }

    public class Item:CommonItem
    {
        public string id { get; set; }
        public Snippet snippet { get; set; }
        public Contentdetails contentDetails { get; set; }
    }

    public class Snippet:CommonSnippet
    {
        public Resourceid resourceId { get; set; }
    }

    public class Resourceid
    {
        public string kind { get; set; }
        public string channelId { get; set; }
    }   

    public class Contentdetails
    {
        public int totalItemCount { get; set; }
        public int newItemCount { get; set; }
        public string activityType { get; set; }
    }

}
