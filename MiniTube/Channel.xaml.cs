﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// O modelo de item de Controle de Usuário está documentado em https://go.microsoft.com/fwlink/?LinkId=234236

namespace MiniTube
{
    public sealed partial class Channel : Page
    {
        public Channel()
        {
            this.InitializeComponent();


            List();
        }

        public async void List()
        {
            YouTube.API.Parameters.SearchParameters parameters = new YouTube.API.Parameters.SearchParameters();
            parameters.part = "id";
            parameters.relatedToVideoId = "n1AVM-JZams";
            parameters.eventType = "completed";
            parameters.type = "video";
            var result = await YouTube.API.Search.List(parameters);

            var ids = result.items.Select(x => x.Id.VideoId);
            string s = "";
            foreach (var id in ids)
            {
                s += id + ",";
            }
            s = s.Substring(0, s.Length - 1);

            YouTube.API.Parameters.VideosParameters videoParameter = new YouTube.API.Parameters.VideosParameters();
            videoParameter.part = "snippet,statistics,contentDetails";
            videoParameter.fields = "items(contentDetails(definition,dimension,duration,licensedContent),id,snippet(channelId,channelTitle,description,localized,publishedAt,thumbnails,title),statistics)";
            videoParameter.id = s;

            var rst = await YouTube.API.Videos.List(videoParameter);

            player.RelatedVideos = rst.Items;

            player.SetVideo("G0mpGYNnRHM");
            
        }
    }
}
