﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTube.API.OAuth2;

namespace MiniTube
{
    public static class Login
    {
        static Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
        public static bool isLoggedIn
        {
            get
            {
                return localSettings.Values["RefAccessToken"] != null;
            }
        }
        public static UserInfo UserInfoData { get; set; }


        private static string _refToken;
        public static string RefreshToken
        {
            get
            {
                var t = localSettings.Values["RefAccessToken"];

                return t == null ? _refToken : t.ToString();

            }
            set { localSettings.Values["RefAccessToken"] = _refToken = value; }
        }
        public static void Logout()
        {
            localSettings.Values["RefAccessToken"] = null;
        }
        public static async Task<AccessToken> GetToken()
        {
            Authentication u = new Authentication();
            Profile pf = new Profile();
            if (RefreshToken == null)
            {
                var tk = await u.Login();
                var user = await pf.GetUserInfo(tk);
                Logged(user, tk);
                if (tk.refresh_token != null)
                    RefreshToken = tk.refresh_token;
                return tk;
            }
            else
            {
                var tk = await u.Login(RefreshToken);
                var user = await pf.GetUserInfo(tk);
                Logged(user, tk);
                if (tk.refresh_token != null)
                    RefreshToken = tk.refresh_token;
                return tk;
            }
        }
        public static async Task<AccessToken> GetTokenIfLoggedIn()
        {
            Authentication u = new Authentication();
            Profile pf = new Profile();
            if (RefreshToken != null)
            {
                var tk = await u.Login(RefreshToken);
                var user = await pf.GetUserInfo(tk);
                Logged(user, tk);
                if (tk.refresh_token != null)
                    RefreshToken = tk.refresh_token;
                return tk;
            }
            else
            {
                return null;
            }
        }
        public static event EventHandler<LoginEventArgs> LoggedIn;

        private static void Logged(UserInfo userinfo, AccessToken token)
        {
            if (LoggedIn != null)
            {
                LoggedIn(null, new LoginEventArgs(userinfo, token));
            }
        }        
    }
    public class LoginEventArgs : EventArgs
    {
        public LoginEventArgs(UserInfo info, AccessToken token)
        {
            this.UserInfo = info;
            this.Token = token;
        }
        public AccessToken Token { get; set; }
        public UserInfo UserInfo { get; set; }
    }
}

