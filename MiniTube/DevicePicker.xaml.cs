﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.Enumeration;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media.Casting;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace MiniTube
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DevicePicker : Page
    {
        public DevicePicker()
        {
            this.InitializeComponent();

            startWatcherButton_Click(this, null);
        }

        DeviceWatcher deviceWatcher;
        CastingConnection castingConnection;

        private void startWatcherButton_Click(object sender, RoutedEventArgs e)
        {
            startWatcherButton.IsEnabled = false;
            watcherProgressRing.IsActive = true;

            castingDevicesListBox.Items.Clear();

            //Create our watcher and have it find casting devices capable of video casting
            deviceWatcher = DeviceInformation.CreateWatcher(CastingDevice.GetDeviceSelector(CastingPlaybackTypes.Video));

            //Register for watcher events
            deviceWatcher.Added += DeviceWatcher_Added;
            deviceWatcher.Removed += DeviceWatcher_Removed;
            deviceWatcher.EnumerationCompleted += DeviceWatcher_EnumerationCompleted;
            deviceWatcher.Stopped += DeviceWatcher_Stopped;
        }

        private async void DeviceWatcher_Added(DeviceWatcher sender, DeviceInformation args)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
            {
                //Add each discovered device to our listbox
                CastingDevice addedDevice = await CastingDevice.FromIdAsync(args.Id);
                castingDevicesListBox.Items.Add(addedDevice);
            });
        }
        private async void DeviceWatcher_Removed(DeviceWatcher sender, DeviceInformationUpdate args)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                foreach (CastingDevice currentDevice in castingDevicesListBox.Items)
                {
                    if (currentDevice.Id == args.Id)
                    {
                        castingDevicesListBox.Items.Remove(currentDevice);
                    }
                }
            });
        }
        private async void DeviceWatcher_EnumerationCompleted(DeviceWatcher sender, object args)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                //If enumeration completes, update UI and transition watcher to the stopped state
                //ShowMessageToUser("Watcher completed enumeration of devices");
                deviceWatcher.Stop();
            });
        }
        private async void DeviceWatcher_Stopped(DeviceWatcher sender, object args)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                //Update UX when the watcher stops
                startWatcherButton.IsEnabled = true;
                watcherProgressRing.IsActive = false;
            });
        }
        private void castingDevicesListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (castingDevicesListBox.SelectedItem != null)
            {
                //When a device is selected, first thing we do is stop the watcher so it's search doesn't conflict with streaming
                if (deviceWatcher.Status != DeviceWatcherStatus.Stopped)
                {
                    deviceWatcher.Stop();
                }

                //Create a new casting connection to the device that's been selected
                castingConnection = ((CastingDevice)castingDevicesListBox.SelectedItem).CreateCastingConnection();

                //Register for events
                castingConnection.ErrorOccurred += Connection_ErrorOccurred;
                castingConnection.StateChanged += Connection_StateChanged;

                DeviceSelected?.Invoke(this, new DeviceSelectedEventArgs() { Connection = castingConnection });

                //Cast the loaded video to the selected casting device.
                //await castingConnection.RequestStartCastingAsync(mediaPlayerElement.MediaPlayer.GetAsCastingSource());
                disconnectButton.Visibility = Visibility.Visible;
            }
        }

        public event EventHandler<DeviceSelectedEventArgs> DeviceSelected;

        private async void Connection_StateChanged(CastingConnection sender, object args)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                //Update the UX based on the casting state
                if (sender.State == CastingConnectionState.Connected || sender.State == CastingConnectionState.Rendering)
                {
                    disconnectButton.Visibility = Visibility.Visible;
                    watcherProgressRing.IsActive = false;
                }
                else if (sender.State == CastingConnectionState.Disconnected)
                {
                    disconnectButton.Visibility = Visibility.Collapsed;
                    castingDevicesListBox.SelectedItem = null;
                    watcherProgressRing.IsActive = false;
                }
                else if (sender.State == CastingConnectionState.Connecting)
                {
                    disconnectButton.Visibility = Visibility.Collapsed;
                    //ShowMessageToUser("Connecting");
                    watcherProgressRing.IsActive = true;
                }
                else
                {
                    //Disconnecting is the remaining state
                    disconnectButton.Visibility = Visibility.Collapsed;
                    watcherProgressRing.IsActive = true;
                }
            });
        }
        private async void Connection_ErrorOccurred(CastingConnection sender, CastingConnectionErrorOccurredEventArgs args)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                //Clear the selection in the listbox on an error
                //ShowMessageToUser("Casting Error: " + args.Message);
                castingDevicesListBox.SelectedItem = null;
            });
        }
        private async void disconnectButton_Click(object sender, RoutedEventArgs e)
        {
            if (castingConnection != null)
            {
                //When disconnect is clicked, the casting conneciton is disconnected.  The video should return locally to the media element.
                await castingConnection.DisconnectAsync();
            }
        }
    }
    public class DeviceSelectedEventArgs
    {
        public CastingConnection Connection { get; set; }
    }
    
}
