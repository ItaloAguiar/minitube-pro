﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace MiniTube
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SearchFilter : Page
    {
        public SearchFilter()
        {
            this.InitializeComponent();
        }
        public delegate void QuerySubmittedHandler(SearchFilter sender, YouTube.API.Parameters.SearchParameters p);
        public event QuerySubmittedHandler QuerySubmitted;

        YouTube.API.Parameters.SearchParameters parameter = new YouTube.API.Parameters.SearchParameters();
        private void ToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            _content.Height = double.NaN;
        }

        private void ToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            _content.Height = 0;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox b = sender as ComboBox;

            if(b != null)
            {
                if(b.SelectedItem != null)
                {
                    parameter.GetType().GetProperty(b.Tag.ToString()).SetValue(parameter, (b.SelectedItem as ComboBoxItem).Tag, null);
                }
            }
            GenerateFilter();
        }

        private void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            l4.SelectedItem = null;
        }
        string previousQuery = string.Empty;
        private async void SearchBox_SuggestionsRequested(SearchBox sender, SearchBoxSuggestionsRequestedEventArgs e)
        {
            var deferral = e.Request.GetDeferral();
            try
            {
                string queryText = e.QueryText;
                if (!string.IsNullOrEmpty(queryText) && previousQuery != queryText)
                {
                    Windows.ApplicationModel.Search.SearchSuggestionCollection suggestionCollection = e.Request.SearchSuggestionCollection;
                    var suggestions = await YouTube.API.Search.Suggest(queryText);
                    foreach (string suggestion in suggestions)
                    {
                        if (suggestion.StartsWith(queryText, StringComparison.CurrentCultureIgnoreCase))
                        {
                            suggestionCollection.AppendQuerySuggestion(suggestion);
                        }
                    }
                    previousQuery = queryText;
                }
            }
            finally
            {
                deferral.Complete();
            }
        }
        private void GenerateFilter()
        {

            if (QuerySubmitted != null)
                QuerySubmitted(this, parameter);
        }

        private void SearchBox_QuerySubmitted(SearchBox sender, SearchBoxQuerySubmittedEventArgs args)
        {
            parameter.q = args.QueryText;
            GenerateFilter();
        }

        private void ToggleSwitch_Toggled(object sender, RoutedEventArgs e)
        {
            if ((sender as ToggleSwitch).IsOn)
                parameter.safeSearch = YouTube.API.Parameters.SearchParameters.SafeSearch.strict;
            else
                parameter.safeSearch = YouTube.API.Parameters.SearchParameters.SafeSearch.moderate;
        }
    }
}
