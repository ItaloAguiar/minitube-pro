﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Media.Core;
using Windows.Media.Playback;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MiniTube
{
    public class PIPManager
    {
        static PIPManager()
        {
            applicationView = CoreApplication.CreateNewView();
            

            Initialize();
        }

        private static void ApplicationView_HostedViewClosing(CoreApplicationView sender, HostedViewClosingEventArgs args)
        {
            
        }

        private static int ViewId;
        private static CoreApplicationView applicationView;
        private static MediaPlayerElement MediaPlayer;

        public static bool IsClosed { get; private set; }

        private static async void Initialize()
        {
            await applicationView.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                ViewId = ApplicationView.GetForCurrentView().Id;

                MediaPlayer = new MediaPlayerElement();
                MediaPlayer.AreTransportControlsEnabled = true;
                MediaPlayer.TransportControls = new YouTube.UI.YouTubePlayerTransportControls();
                MediaPlayer.SetMediaPlayer(new MediaPlayer());

                
                applicationView.TitleBar.ExtendViewIntoTitleBar = true;

            });
        }

        public static async Task<TimeSpan> GetPosition()
        {
            TimeSpan ts = default(TimeSpan);
            await applicationView.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                ts = MediaPlayer.MediaPlayer.PlaybackSession.Position;
            });
            return ts;
        }

        public static async void ShowPIP(Uri Source, TimeSpan position )
        {
            await applicationView.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                MediaPlayer.MediaPlayer.Source = MediaSource.CreateFromUri(Source);
                MediaPlayer.MediaPlayer.PlaybackSession.Position = position;

                MediaPlayer.MediaPlayer.Play();
            });

            Show();
        }

        public static async void Show()
        {
            try
            {
                await applicationView.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    Window.Current.VisibilityChanged += (s, a) =>
                    {
                        if (!a.Visible)
                        {

                            MediaPlayer.MediaPlayer.Pause();
                        }
                    };
                    Window.Current.Content = MediaPlayer;
                    Window.Current.Activate();

                });

                IsClosed = !await ApplicationViewSwitcher.TryShowAsViewModeAsync(ViewId, ApplicationViewMode.CompactOverlay);
            }
            catch { }

        }

        public static async void Play()
        {
            await applicationView.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                MediaPlayer.MediaPlayer.Play();
            });
        }

        public static async void Pause()
        {
            await applicationView.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                MediaPlayer.MediaPlayer.Pause();
            });
        }

        public static async void ConsolidateViews(int currentViewId)
        {
            try
            {
                await ApplicationViewSwitcher.SwitchAsync(currentViewId, ViewId, ApplicationViewSwitchingOptions.ConsolidateViews);

                Pause();

            }
            catch { }
        }

        
    }
}
