﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using YouTube.API;
using YouTube.API.DataModel.Search;
using YouTube.API.DataModel.Videos;
using YouTube.API.OAuth2;
using YouTube.API.Parameters;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace MiniTube
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            Login.LoggedIn += Login_LoggedIn;
            searchFilter.QuerySubmitted += (sender, args) =>
            {
                Search(args);
            };
            

            //CoreApplicationViewTitleBar coreTitleBar = CoreApplication.GetCurrentView().TitleBar;
            //SearchBox b = new SearchBox();
            //b.HorizontalAlignment = HorizontalAlignment.Right;

            //coreTitleBar.ExtendViewIntoTitleBar = true;
            //Window.Current.SetTitleBar(b);

            SetContent(typeof(Channel),null);

            Window.Current.SetTitleBar(titleBar);
        }
        ~MainPage()
        {
            Login.LoggedIn -= Login_LoggedIn;            
        }

        


        LocalVideoParamenter parameter;
        SearchFilter searchFilter = new SearchFilter();
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter != null && e.Parameter.ToString().StartsWith("video"))
            {
                try
                {
                    IsLoading = true;
                    parameter = new LocalVideoParamenter();
                    var video = await Videos.List(new YouTube.API.Parameters.VideosParameters()
                    {
                        part = "snippet,statistics",
                        id = e.Parameter.ToString().Split('=').LastOrDefault()
                    });
                    parameter.Video = video.Items[0];
                }
                finally { IsLoading = false; }
            }
            this.RegisterBackgroundTask();
        }


        private  void RegisterBackgroundTask()
        {
            //var backgroundAccessStatus = await BackgroundExecutionManager.RequestAccessAsync();
            //if (backgroundAccessStatus == BackgroundAccessStatus.AllowedMayUseActiveRealTimeConnectivity ||
            //    backgroundAccessStatus == BackgroundAccessStatus.AllowedWithAlwaysOnRealTimeConnectivity)
            //{
            //    foreach (var task in BackgroundTaskRegistration.AllTasks)
            //    {
            //        if (task.Value.Name == taskName)
            //        {
            //            task.Value.Unregister(true);
            //        }
            //    }

            //    BackgroundTaskBuilder taskBuilder = new BackgroundTaskBuilder();
            //    taskBuilder.AddCondition(new SystemCondition(SystemConditionType.InternetAvailable));
            //    taskBuilder.Name = taskName;
            //    taskBuilder.TaskEntryPoint = taskEntryPoint;
            //    taskBuilder.SetTrigger(new TimeTrigger(360, false));
            //    var registration = taskBuilder.Register();
            //}
        }

        private const string taskName = "VideoUpdater";
        private const string taskEntryPoint = "BackgroundTasks.VideoUpdater";

        public async Task<List<StandardFeed>> LoadFeed(string chart = "mostPopular", string categoryId = null, string title = "Popular on YouTube")
        {

            var token = await Login.GetTokenIfLoggedIn();

            List<StandardFeed> feeds = new List<StandardFeed>();

            var feed = await Videos.List( 
                new YouTube.API.Parameters.VideosParameters()
                {
                    chart = chart,
                    part = "snippet,statistics,contentDetails",
                    regionCode = System.Globalization.RegionInfo.CurrentRegion.Name,
                    maxResults = 50,
                    videoCategoryId = categoryId                    
                }
            );
            

            feeds.Add(new StandardFeed()
            {
                Name = title,
                /*Image = new BitmapImage(new Uri("ms-appx:///Assets/YPopular.jpg", UriKind.Absolute)),*/
                Feed = feed.Items
            });

            if (categoryId == null && token != null)
            {
                var f = await (new YouTube.API.Activities()).List(token, new ActivitiesParameters() {
                    home = true,
                    part = "contentDetails",
                    maxResults = 50,
                    regionCode = "br"
                });

                var ids = f.Items.Select(x => x.ContentDetails.Upload.VideoId).ToArray();
                string s = "";
                foreach (var id in ids)
                {
                    s += id + ",";
                }
                s = s.Substring(0, s.Length - 1);

                VideosParameters videoParameter = new VideosParameters();
                videoParameter.part = "snippet,statistics,contentDetails";
                videoParameter.fields = "items(contentDetails(definition,dimension,duration,licensedContent),id,snippet(channelId,channelTitle,description,localized,publishedAt,thumbnails,title),statistics)";
                videoParameter.id = s;

                var rst = await YouTube.API.Videos.List(videoParameter);

                feeds.Add(new StandardFeed()
                {
                    Name = "Vídeos recomendados",
                    /*Image = new BitmapImage(new Uri("ms-appx:///Assets/YPopular.jpg", UriKind.Absolute)),*/
                    Feed = rst.Items
                });
            }

            ContentContainer.Navigate(typeof(Home), feeds);

            await Task.Delay(200);

            if (parameter != null)
                ContentContainer.Navigate(typeof(View), parameter);

            parameter = null;


            return feeds;
        }
        public async Task<List<StandardFeed>> LoadLiked(string title = "Liked Videos")
        {
            List<StandardFeed> feeds = new List<StandardFeed>();

            var feed = new YouTube.API.Util.IncrementalLoader<VideosResponse, VideosParameters>(
                async (p)=> 
                {
                    return await Videos.List(await Login.GetToken(), p);
                }, 
                new YouTube.API.Parameters.VideosParameters()
                {
                    part = "snippet,statistics",
                    maxResults = 50,
                    myRating = "like"
                },
                500
            );
            feeds.Add(new StandardFeed()
            {
                Name = title,
                /*Image = new BitmapImage(new Uri("ms-appx:///Assets/YPopular.jpg", UriKind.Absolute)),*/
                Feed = feed
            });

            ContentContainer.Navigate(typeof(Home), feeds);

            await Task.Delay(1000);

            if (parameter != null)
                ContentContainer.Navigate(typeof(View), parameter);

            parameter = null;
            return feeds;
        }
        public async Task<List<StandardFeed>> LoadSubscriptionFeed()
        {
            List<StandardFeed> feeds = new List<StandardFeed>();
            try
            {
                var token = await Login.GetToken();

                IsLoading = true;
                YouTube.API.OAuth2.Profile p = new YouTube.API.OAuth2.Profile();
                var channels = await YouTube.API.Subscriptions.List(token, new YouTube.API.Parameters.SubscriptionsParameters()
                {
                    channelId = await p.GetUserId(token),
                    maxResults = 50
                });
                foreach (var item in channels.items)
                {
                    try
                    {
                        feeds.Add(new StandardFeed()
                        {
                            Name = item.snippet.Title,
                            /*Image = new BitmapImage(new Uri("ms-appx:///Assets/YPopular.jpg", UriKind.Absolute)),*/
                            Feed = (await YouTube.API.Search.List(new YouTube.API.Parameters.SearchParameters()
                            {
                                channelId = item.snippet.resourceId.channelId,
                                order = YouTube.API.Parameters.SearchParameters.OrderType.date,
                                maxResults = 14,
                                type = "video",
                                videoSyndicated = "true"

                            }
                        )).items.ToObservableCollection()
                        });
                    }
                    catch { }
                }
                SetContent(typeof(Home), feeds);
            }
            finally
            {
                IsLoading = false;
            }

            return feeds;
        }
        public void Search(string text)
        {
            try
            {
                GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Search", text, 1);
                ResourceLoader LanguageLoader = new Windows.ApplicationModel.Resources.ResourceLoader();
                StandardFeed feed = (new StandardFeed()
                {
                    Name = string.Format(LanguageLoader.GetString("SearchResults"), text),
                    /*Image = new BitmapImage(new Uri("ms-appx:///Assets/YPopular.jpg", UriKind.Absolute)),*/
                    Feed = new YouTube.API.Util.IncrementalLoader<SearchResponse, SearchParameters>(YouTube.API.Search.List, new SearchParameters()
                    {
                        q = text,
                        maxResults = 40,
                        type = "video",
                        videoSyndicated = "true"
                        //safeSearch = YouTube.API.Parameters.SearchParameters.SafeSearch.strict                      

                    })
                });

                ContentContainer.Navigate(typeof(Search), feed);
            }
            catch { }
        }
        public void Search(SearchParameters parameter)
        {
            try
            {
                GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Search", parameter.q, 1);
                ResourceLoader LanguageLoader = new Windows.ApplicationModel.Resources.ResourceLoader();
                StandardFeed feed = (new StandardFeed()
                {
                    Name = string.Format(LanguageLoader.GetString("SearchResults"), parameter.q),
                    /*Image = new BitmapImage(new Uri("ms-appx:///Assets/YPopular.jpg", UriKind.Absolute)),*/
                    Feed = new YouTube.API.Util.IncrementalLoader<SearchResponse, SearchParameters>(YouTube.API.Search.List, parameter)
                });

                ContentContainer.Navigate(typeof(Search), feed);
            }
            catch { }
        }
        public void SetContent(Type u, object parameter)
        {
            ContentContainer.Navigate(u, parameter);
        }

        public async Task<T> RunAsync<T>(Func<T> func)
        {
            overlay.Visibility = Visibility.Visible;

            T val = await Task.Run<T>(() =>
            {
                return func();
            });

            overlay.Visibility = Visibility.Collapsed;

            return val;
                 
        }

        public bool IsLoading
        {
            set
            {
                try
                {
                    progress.IsIndeterminate = value;

                    overlay.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
                }
                catch { }
            }
            get { return progress.IsIndeterminate; }
        }
        public static MainPage GetInstance()
        {
            var frame = (Frame)Window.Current.Content;
            return (MainPage)frame.Content;
        }
        public Frame GetContainer()
        {
            return ContentContainer;
        }
        object home, news, sports, movies, games, music, subscriptions, like;

        

        private void player_PointerPressed(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            (sender as MediaPlayerElement).MediaPlayer.Pause();
        }

        private void player_RightTapped(object sender, Windows.UI.Xaml.Input.RightTappedRoutedEventArgs e)
        {
            var r = ContentContainer.BackStack.Where(x => x.SourcePageType == typeof(View)).FirstOrDefault();
            
        }

        private void GoBack_Click(object sender, RoutedEventArgs e)
        {
            ContentContainer.GoBack();
        }

        #region Login
        private async void Login_Requested(object sender, RoutedEventArgs e)
        {
            try
            {
                await Login.GetToken();
            }
            catch { }
        }
        void Login_LoggedIn(object sender, LoginEventArgs e)
        {
            SetUserInfoLayout(e.UserInfo);
        }
        UserInfo userinfo;
        private void SetUserInfoLayout(UserInfo info)
        {
            LoginButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            if (info.name != null)
                Username.Text = info.name;
            if (info.picture != null)
                UserPicture.Fill = new ImageBrush() {ImageSource = new BitmapImage(new System.Uri(info.picture))};  
            userinfo = info;
        }
        #endregion

        private void ToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            CloseMenu.Begin();
        }

        private void ToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            CloseSecondaryMenu.Begin();
            OpenMenu.Begin();
        }

        private async void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = (sender as ListView).SelectedItem as ListViewItem;
            IsLoading = true;
            try
            {
                if (item != null)
                {
                    ResourceLoader LanguageLoader = new Windows.ApplicationModel.Resources.ResourceLoader();
                    switch (item.Tag.ToString())
                    {
                        case "home":
                            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Popular", null, 1);
                            if (home == null)
                                home = await LoadFeed("mostPopular", null, LanguageLoader.GetString("Popular/Label"));
                            else
                                ContentContainer.Navigate(typeof(Home), home);
                            break;
                        case "subscriptions":
                            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Subscriptions Feed", null, 1);
                            if (subscriptions == null)
                            {
                                subscriptions = await LoadSubscriptionFeed();
                                try
                                {
                                    foreach (var i in (List<StandardFeed>)subscriptions)
                                        ((List<StandardFeed>)home).Add(i);
                                }
                                catch { }
                            }
                            else ContentContainer.Navigate(typeof(Home), subscriptions);
                            break;
                        case "liked":
                            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Liked Videos", null, 1);
                            if (like == null)
                                like = await LoadLiked();
                            else ContentContainer.Navigate(typeof(Home), like);
                            break;
                        case "music":
                            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Music", null, 1);
                            if (music == null)
                                music = await LoadFeed("mostPopular", "10", LanguageLoader.GetString("Music/Label"));
                            else ContentContainer.Navigate(typeof(Home), music);
                            break;
                        case "sports":
                            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Sports", null, 1);
                            if (sports == null)
                                sports = await LoadFeed("mostPopular", "17", LanguageLoader.GetString("Sports/Label"));
                            else ContentContainer.Navigate(typeof(Home), sports);
                            break;
                        case "movies":
                            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Movies", null, 1);
                            if (movies == null)
                                movies = await LoadFeed("mostPopular", "1", LanguageLoader.GetString("Movies/Label"));
                            else ContentContainer.Navigate(typeof(Home), movies);
                            break;
                        case "news":
                            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "News", null, 1);
                            if (news == null)
                                news = await LoadFeed("mostPopular", "25", LanguageLoader.GetString("News/Label"));
                            else ContentContainer.Navigate(typeof(Home), news);
                            break;
                        case "games":
                            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Games", null, 1);
                            if (games == null)
                                games = await LoadFeed("mostPopular", "20", LanguageLoader.GetString("Games/Label"));
                            else ContentContainer.Navigate(typeof(Home), games);
                            break;
                        case "settings":
                            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Settings", null, 1);
                            //SettingsPane.Show();
                            (sender as ListView).SelectedItem = null;
                            break;
                        case "search":
                            ContentContainer.Navigate(typeof(Search));
                            break;
                    }

                }
            }
            catch { }
            finally
            {
                IsLoading = false;
            }
        }
    }
    public static class LinqExtensions
    {
        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> items)
        {
            return new ObservableCollection<T>(items);
        }
    }
    public class LocalVideoParamenter
    {
        public object Video { get; set; }
        public object RelatedVideos { get; set; }
        public object SearchVideo { get; set; }
        public object Playlist { get; set; }
    }
    public class StandardFeed
    {
        public StandardFeed()
        {
            Feed = new object();
        }
        public string Name { get; set; }
        public ImageSource Image { get; set; }
        public object Feed { get; set; }
    }
    public class ResourceStringConventer : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            ResourceLoader resourceLoader = new ResourceLoader();
            string text = resourceLoader.GetString((string)parameter);
            return text;
        }
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
