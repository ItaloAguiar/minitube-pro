﻿using System;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;
using YouTube.API;
using YouTube.API.DataModel.Search;
using YouTube.API.Parameters;
using System.Linq;
using Windows.UI.Xaml.Media.Animation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace MiniTube
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Search : Page
    {
        public Search()
        {
            this.InitializeComponent();
        }

        private object item;

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //var item = e.Parameter as StandardFeed;
            //mainGridView.ItemsSource = item.Feed;

        }
        private async void mainGridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            LocalVideoParamenter p = new LocalVideoParamenter();
            if (e.ClickedItem is YouTube.API.DataModel.Search.Item)
            {
                var item = e.ClickedItem as YouTube.API.DataModel.Search.Item;
                if (item.Id.Kind == "youtube#playlist")
                {
                    var data = await PlaylistItems.List(new YouTube.API.Parameters.PlaylistItemsParemeters()
                    {
                        part = "snippet",
                        playlistId = item.Id.PlaylistId
                    });
                    Frame.Navigate(typeof(View), data.items);
                    return;
                }
                else
                    p.SearchVideo = item;
            }
            if (e.ClickedItem is YouTube.API.DataModel.Videos.Item)
                p.Video = e.ClickedItem;

            if (mainGridView.ContainerFromItem(e.ClickedItem) is GridViewItem)
            {
                item = e.ClickedItem;

                mainGridView.PrepareConnectedAnimation("controlAnimation", e.ClickedItem, "controlRoot");

            }

            Frame.Navigate(typeof(View), p);
        }
        public void SearchVideos(string text)
        {
            try
            {
                GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Search", text, 1);
                ResourceLoader LanguageLoader = new Windows.ApplicationModel.Resources.ResourceLoader();
                StandardFeed feed = (new StandardFeed()
                {
                    Name = string.Format(LanguageLoader.GetString("SearchResults"), text),
                    /*Image = new BitmapImage(new Uri("ms-appx:///Assets/YPopular.jpg", UriKind.Absolute)),*/
                    Feed = new YouTube.API.Util.IncrementalLoader<SearchResponse, SearchParameters>(async (p) => 
                    {
                        var r = await YouTube.API.Search.List(p);
                        r.items = r.items.Where(x => x.Snippet.LiveBroadcastContent == "none").ToArray();
                        return r;

                    }, new SearchParameters()
                    {
                        q = text,
                        maxResults = 40,
                        type = "video",
                        videoSyndicated = "true"
                        //safeSearch = YouTube.API.Parameters.SearchParameters.SafeSearch.strict                        

                    }, 500)
                });

                mainGridView.ItemsSource = feed.Feed;

            }
            catch { }
        }

        public void SearchVideos(SearchParameters parameter)
        {
            try
            {
                GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Home", "Search", parameter.q, 1);
                ResourceLoader LanguageLoader = new Windows.ApplicationModel.Resources.ResourceLoader();
                StandardFeed feed = (new StandardFeed()
                {
                    Name = string.Format(LanguageLoader.GetString("SearchResults"), parameter.q),
                    /*Image = new BitmapImage(new Uri("ms-appx:///Assets/YPopular.jpg", UriKind.Absolute)),*/
                    Feed = new YouTube.API.Util.IncrementalLoader<SearchResponse, SearchParameters>(YouTube.API.Search.List, parameter)
                });

                mainGridView.ItemsSource = feed.Feed;
            }
            catch { }
        }
        private void Header_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                //SearchVideos((sender as TextBox).Text);
            }
        }

        private async void mainGridView_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            ConnectedAnimation animation = ConnectedAnimationService.GetForCurrentView().GetAnimation("controlAnimation");

            if (animation != null)
            {
                try
                {
                    await mainGridView.TryStartConnectedAnimationAsync(animation, item, "controlRoot");
                }
                catch { }
            }
        }

        private void Header_QuerySubmitted(SearchBox sender, SearchBoxQuerySubmittedEventArgs args)
        {
            SearchVideos(args.QueryText);
        }
    }
}
