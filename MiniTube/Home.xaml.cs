﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using YouTube.API;
using YouTube.UI;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace MiniTube
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Home : Page
    {
        public Home()
        {
            this.InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            feedsCollection.Source = e.Parameter;

            var p = e.Parameter as List<StandardFeed>;


            lv.ItemsSource = p;

            //if(p != null)
            //{
            //    if(p.Count == 1 && p[0].Feed is ObservableCollection<object>)
            //    {
            //        mainGridView.ItemsSource = p[0].Feed;
            //        mainGridView.Header = p[0];
            //    }
            //}
        }

        private object item;

        private async void itemsFlipView_ItemClick(object sender, ItemClickEventArgs e)
        {
            ItemsFlipView ifv = sender as ItemsFlipView;

            LocalVideoParamenter p = new LocalVideoParamenter();
            if (e.ClickedItem is YouTube.API.DataModel.Search.Item)
            {
                var item = e.ClickedItem as YouTube.API.DataModel.Search.Item;
                if (item.Id.Kind == "youtube#playlist")
                {
                    var data = await PlaylistItems.List(new YouTube.API.Parameters.PlaylistItemsParemeters()
                    {
                        part = "snippet",
                        playlistId = item.Id.PlaylistId
                    });
                    //Frame.Navigate(typeof(ViewPlaylist), data.items);
                    return;
                }
                else
                    p.SearchVideo = item;
            }
            if (e.ClickedItem is YouTube.API.DataModel.Videos.Item)
                p.Video = e.ClickedItem;

            if (ifv.ContainerFromItem(e.ClickedItem) is ListViewItem)
            {
                item = e.ClickedItem;

                ifv.PrepareConnectedAnimation("controlAnimation", e.ClickedItem, "controlRoot");

            }

            Frame.Navigate(typeof(View), p);
        }

        private async void itemsFlipView_Loaded(object sender, RoutedEventArgs e)
        {
            ConnectedAnimation animation = ConnectedAnimationService.GetForCurrentView().GetAnimation("controlAnimation");

            if (animation != null)
            {
                try
                {
                    await (sender as ItemsFlipView).TryStartConnectedAnimationAsync(animation, item, "controlRoot");
                }
                catch { }
            }
        }
    }
    public class TileTemplateSelector : DataTemplateSelector
    {
        //These are public properties that will be used in the Resources section of the XAML.
        public DataTemplate VideoTemplate { get; set; }
        public DataTemplate PlaylistTemplate { get; set; }

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            var i = item as YouTube.API.DataModel.CommonItem;
            if (i.Kind == "youtube#searchResult")
            {
                var sitem = i as YouTube.API.DataModel.Search.Item;
                if (sitem.Id.Kind == "youtube#playlist")
                    return PlaylistTemplate;
                else return VideoTemplate;
            }
            else return VideoTemplate;
        }
    }
}
