﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.Media.Casting;
using Windows.Media.Playback;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using YouTube.API;
using YouTube.API.DataModel.CommentThreads;
using YouTube.API.Parameters;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace MiniTube
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class View : Page
    {

        public View()
        {
            this.InitializeComponent();

            //dp.Filter.SupportedCastingSources.Add(player.GetAsCastingSource());
            dp.Filter.SupportsVideo = true;            


            Misc.MenuIndexService.SetIndex(-1);
            
            //Window.Current.CoreWindow.VisibilityChanged += async (s, a) =>
            //{
            //    TimeSpan position = player.MediaPlayer.PlaybackSession.Position;

            //    if (!s.Visible)
            //    {
            //        player.MediaPlayer.Pause();

            //        try
            //        {
            //            PIPManager.ShowPIP(((MediaSource)Player.Source).Uri, position);
            //        }
            //        catch { }
                    
            //    }
            //    else
            //    {
            //        if (!PIPManager.IsClosed)
            //        {
            //            position = await PIPManager.GetPosition();
            //            player.MediaPlayer.PlaybackSession.Position = position;
            //            player.MediaPlayer.Play();
            //        }


            //        PIPManager.ConsolidateViews(ApplicationView.GetForCurrentView().Id);
                    
            //    }
            //};
        }
        CastingDevicePicker dp = new CastingDevicePicker();

        static MediaPlayer mPlayer;
        public static MediaPlayer Player
        {
            get { if (mPlayer == null) mPlayer = new MediaPlayer(); return mPlayer; }
            set { mPlayer = value; }
        }

        
        YouTube.API.DataModel.CommonItem NextVid;
        YouTube.API.DataModel.Videos.VideosResponse RelatedContent;

        public YouTube.API.DataModel.Videos.Item Video { get; set; }
        public YouTube.API.Util.IncrementalLoader<CommentThreadsResponse, CommentThreadsParameters> Comments { get; set; }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            rootControl.ChangeView(0, 0, null);

            var connectedAnimation = ConnectedAnimationService.GetForCurrentView().GetAnimation("controlAnimation");

            if (connectedAnimation != null)
            {   
                connectedAnimation.TryStart(player, new UIElement[] { rootControl });
            }

            var item = e.Parameter as LocalVideoParamenter;
            Video = item.Video as YouTube.API.DataModel.Videos.Item;
            Load(item);
            //autoPlay.IsOn = localSettings.Values["AutoPlay"] == null || localSettings.Values["AutoPlay"].ToString() == "On";
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            ConnectedAnimationService.GetForCurrentView().PrepareToAnimate("controlAnimation", rootControl);
            base.OnNavigatingFrom(e);
        }

        string videoId = "";
        private async void Load(LocalVideoParamenter item)
        {
            if (item.SearchVideo != null)
            {
                try
                {
                    videoId = (item.SearchVideo as YouTube.API.DataModel.Search.Item).Id.VideoId;
                    item.Video = (await Videos.List(new YouTube.API.Parameters.VideosParameters()
                    {
                        id = videoId,
                        part = "snippet,statistics,contentDetails"
                    })).Items[0];
                }
                catch { }
            }
            else if (item.Playlist != null)
            {
                var playlist = item.Playlist as List<YouTube.API.DataModel.PlaylistItems.Item>;
                try
                {
                    var next = playlist[0];
                    playlist.RemoveAt(0);
                    List<object> o = new List<object>();
                    o.Add(next);
                    NextVid = next;
                    NextVideo.ItemsSource = o;
                    relatedListView.ItemsSource = playlist;
                    return;
                }
                catch { }

            }
            else videoId = (item.Video as YouTube.API.DataModel.Videos.Item).Id;

            Video = item.Video as YouTube.API.DataModel.Videos.Item;

            this.DataContext = this;

            RelatedContent = item.RelatedVideos as YouTube.API.DataModel.Videos.VideosResponse;


            LoadVideo(videoId);


            await LoadRelatedVideos();

            var channel = await Channels.List(new ChannelsParameter()
            {
                part = "snippet",
                id = Video.Snippet.ChannelId
            });

            UserPictureImage.Source = new BitmapImage(channel.items[0].snippet.thumbnails.Default.Url);

            HideLoadingControls();

            //GetRating(videoId);

            LoadComments(videoId);


        }
        private async Task LoadRelatedVideos()
        {
            relatedListView.ItemsSource = null;

            if (RelatedContent == null)
            {
                try
                {
                    YouTube.API.Parameters.SearchParameters parameters = new YouTube.API.Parameters.SearchParameters();
                    parameters.part = "id";
                    parameters.relatedToVideoId = videoId;
                    parameters.eventType = "completed";
                    parameters.type = "video";
                    var result = await YouTube.API.Search.List(parameters);

                    var ids = result.items.Select(x => x.Id.VideoId);
                    string s = "";
                    foreach (var id in ids)
                    {
                        s += id + ",";
                    }
                    s = s.Substring(0, s.Length - 1);

                    YouTube.API.Parameters.VideosParameters videoParameter = new YouTube.API.Parameters.VideosParameters();
                    videoParameter.part = "snippet,statistics,contentDetails";
                    videoParameter.fields = "items(contentDetails(definition,dimension,duration,licensedContent),id,snippet(channelId,channelTitle,description,localized,publishedAt,thumbnails,title),statistics)";
                    videoParameter.id = s;

                    var rst = await YouTube.API.Videos.List(videoParameter);

                    RelatedContent = rst;

                    var related = RelatedContent.Items.ToList();

                    var next = related[0];
                    related.RemoveAt(0);
                    List<object> o = new List<object>();
                    o.Add(next);
                    NextVid = next;
                    NextVideo.ItemsSource = o;

                    relatedListView.ItemsSource = related;
                    related_fail.Visibility = Visibility.Collapsed;
                }
                catch
                {
                    related_tryButton.IsEnabled = true;
                    related_fail.Visibility = Visibility.Visible;
                }
            }
            else
            {
                relatedListView.ItemsSource = RelatedContent.Items;
                related_fail.Visibility = Visibility.Collapsed;
            }
            player.RelatedVideos = RelatedContent.Items;
        }
        private async void LoadComments(string videoId)
        {
            try
            {
                Comments = new YouTube.API.Util.IncrementalLoader<CommentThreadsResponse, CommentThreadsParameters>(
                    YouTube.API.CommentThreads.List, new CommentThreadsParameters()
                    {
                        videoId = videoId,
                        textFormat = "plainText",
                        maxResults = 50,
                        part = CommentThreadsParameters.Part.snippet | CommentThreadsParameters.Part.replies,
                        order = "relevance",
                        fields = "items(id,replies(comments(id,snippet(authorChannelId,authorChannelUrl,authorDisplayName," +
                    "authorProfileImageUrl,likeCount,publishedAt,textDisplay))),snippet(topLevelComment(id,snippet" +
                    "(authorChannelId,authorChannelUrl,authorDisplayName,authorProfileImageUrl,likeCount,publishedAt" +
                    ",textDisplay)),totalReplyCount)),nextPageToken"
                    },
                    await Login.GetTokenIfLoggedIn(), 500
                );

                CommentsList.ItemsSource = Comments;
                

                await CommentsList.LoadMoreItemsAsync();
                
                //var cmts = await YouTube.API.CommentThreads.List(await Login.GetTokenIfLoggedIn(), 
                //    new CommentThreadsParameters()
                //{
                //    videoId = videoId,
                //    textFormat = "plainText",
                //    maxResults = 50,
                //    part = CommentThreadsParameters.Part.snippet | CommentThreadsParameters.Part.replies,
                //    order = "relevance",
                //    fields = "items(id,replies(comments(id,snippet(authorChannelId,authorChannelUrl,authorDisplayName," +
                //    "authorProfileImageUrl,likeCount,publishedAt,textDisplay))),snippet(topLevelComment(id,snippet" +
                //    "(authorChannelId,authorChannelUrl,authorDisplayName,authorProfileImageUrl,likeCount,publishedAt" +
                //    ",textDisplay)),totalReplyCount)),nextPageToken"
                //});
                //CommentsList.Items.Clear();
                //foreach (var c in cmts.items)
                //{
                //    CommentsList.Items.Add(c);
                //}
            }
            catch { }
        }
        private async void LoadVideo(string videoId)
        {
            try
            {
                //InitializeTransportControls();


                player.SetVideo(videoId);                

            }
            catch
            {
                //ImageError.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }

        }

        private async void SubscribeButton_RequestForSubscribe(object sender, YouTube.UI.SubscribeEventArgs e)
        {
            if (SubscribeBtn.IsSubscribed)
            {
                try
                {
                    var token = await Login.GetToken();
                    await YouTube.API.Subscriptions.Delete(token, e.SubscriptionID);
                    e.Success = false;
                    GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Channel", "Unsubscribe", null, 1);
                }
                catch
                {
                    SubscribeBtn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                }
            }
            else
            {
                try
                {
                    var token = await Login.GetToken();
                    var response = await YouTube.API.Subscriptions.Insert(token, e.ChannelID);
                    System.Diagnostics.Debug.WriteLine(response);
                    e.Success = true;
                    e.SubscriptionID = response.id;
                    GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Channel", "Subscribe", null, 1);
                }
                catch (ApiException ex)
                {
                    if (ex.Message.Contains("subscriptionDuplicate")) e.Success = true;
                    else
                    {
                        e.Success = true;
                        e.Success = false;
                    }
                }
            }
        }

        private void ShareBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void View_Web_Click(object sender, RoutedEventArgs e)
        {

        }

        private void CopyLink_Click(object sender, RoutedEventArgs e)
        {

        }

        private void PinToStartMenu_Click(object sender, RoutedEventArgs e)
        {

        }

        private async void LikeBtn_RequestForLike(object sender, YouTube.UI.LikeEventArgs e)
        {
            try
            {
                if (LikeBtn.IsLiked)
                {
                    var token = await Login.GetToken();
                    await YouTube.API.Videos.Rate(token, Video.Id, Videos.Rating.none);
                    e.Success = false;
                    GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Video", "Cancel Like", null, 1);
                }
                else
                {
                    var token = await Login.GetToken();
                    await YouTube.API.Videos.Rate(token, Video.Id, Videos.Rating.like);
                    e.Success = true;
                    GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Video", "Like", null, 1);
                }
            }
            catch { }
        }

        private async void DislikeBtn_RequestForDislike(object sender, YouTube.UI.DislikeEventArgs e)
        {
            try
            {
                if (DislikeBtn.IsDisliked)
                {
                    var token = await Login.GetToken();
                    await YouTube.API.Videos.Rate(token, Video.Id, Videos.Rating.none);
                    e.Success = false;
                    GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Video", "Cancel Dislike", null, 1);
                }
                else
                {
                    var token = await Login.GetToken();
                    await YouTube.API.Videos.Rate(token, Video.Id, Videos.Rating.dislike);
                    e.Success = true;
                    GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Video", "Dislike", null, 1);
                }
            }
            catch { }
        }

        private void nextVideoToggleButton_Toggled(object sender, RoutedEventArgs e)
        {

        }

        private void itemListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            LocalVideoParamenter p = new LocalVideoParamenter();
            p.Video = e.ClickedItem;
            Frame.Navigate(typeof(View), p);
        }

        private void player_RightTapped(object sender, RightTappedRoutedEventArgs e)
        {
            
        }

        private void HideLoadingControls()
        {
            prev1.Visibility = prev2.Visibility = Visibility.Collapsed;
        }



        private void OnPlayerSizeChaged(object sender, SizeChangedEventArgs e)
        {
            var w = e.NewSize.Width / 16;

            (sender as FrameworkElement).Height = (w * 9);
        }

        private async void OnRelatedTryAgain(object sender, RoutedEventArgs e)
        {
            related_tryButton.IsEnabled = false;
            await LoadRelatedVideos();
        }

        private async void OnViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            ScrollViewer sv = (sender as ScrollViewer);            
            if (sv.VerticalOffset >= sv.ScrollableHeight - 200 && Comments.CanLoadMoreItems)
            {
                var r = await CommentsList.LoadMoreItemsAsync();
            }
                
        }

        private async void OnSubmitComment(object sender, YouTube.UI.SubmitCommentEventArgs e)
        {
            YouTube.UI.CommentBox cb = (sender as YouTube.UI.CommentBox);
            try
            {                
                cb.IsEnabled = false;

                var token = await Login.GetToken();

                var response = await CommentThreads.Insert(token, e.Comment, Video.Id);
                Comments.InsertAt(response, 0);
                cb.Clear();
                //GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Video", "Comment", null, 1);
            }
            catch { }
            finally
            {
                cb.IsEnabled = true;
            }
        }

        private async void OnSubmitReply(object sender, YouTube.UI.SubmitCommentEventArgs e)
        {
            YouTube.UI.CommentBox cb = (sender as YouTube.UI.CommentBox);
            try
            {
                cb.IsEnabled = false;

                var token = await Login.GetToken();

                var response = await YouTube.API.Comments.Insert(token, e.Comment, e.TopLevelComment.Id);
                e.TopLevelComment.Replies.Comments.Insert(0,response);
                cb.Clear();
                cb.IsActive = false;
                //GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Video", "Comment", null, 1);
            }
            catch { }
            finally
            {
                cb.IsEnabled = true;
            }
        }
    }
}
