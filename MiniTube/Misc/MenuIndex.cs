﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTube.Misc
{
    public class MenuIndexService
    {
        public static void SetIndex(int val)
        {
            Menu.Value = val;
        }

        public static MenuIndex Menu { get; set; } = new MenuIndex();
    }
    public class MenuIndex: INotifyPropertyChanged
    {
        public int _value;
        public int Value
        {
            get { return _value; }
            set
            {
                _value = value;
                PropertyChanged?.Invoke(this, null);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
