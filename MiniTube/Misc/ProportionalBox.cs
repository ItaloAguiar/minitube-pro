﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MiniTube.Misc
{
    public class ProportionalBox:ContentControl
    {
        public static readonly DependencyProperty ModeProperty = DependencyProperty.Register(
          "Mode",
          typeof(ResizeMode),
          typeof(ProportionalBox),
          new PropertyMetadata(ResizeMode.Horizontal)
        );

        public ResizeMode Mode
        {
            get { return (ResizeMode)GetValue(ModeProperty); }
            set { SetValue(ModeProperty, value); }
        }

        public static readonly DependencyProperty ProportionalWidthProperty = DependencyProperty.Register(
          "ProportionalWidth",
          typeof(double),
          typeof(ProportionalBox),
          new PropertyMetadata(0)
        );

        public double ProportionalWidth
        {
            get { return (double)GetValue(ProportionalWidthProperty); }
            set { SetValue(ProportionalWidthProperty, value); }
        }


        public static readonly DependencyProperty ProportionalHeightProperty = DependencyProperty.Register(
          "ProportionalHeight",
          typeof(double),
          typeof(ProportionalBox),
          new PropertyMetadata(0)
        );

        public double ProportionalHeight
        {
            get { return (double)GetValue(ProportionalHeightProperty); }
            set { SetValue(ProportionalHeightProperty, value); }
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            Size contentSize = new Size();
            if(Mode == ResizeMode.Horizontal)
            {
                var prop = finalSize.Width < ProportionalWidth ? 
                    (finalSize.Width / ProportionalWidth) : 
                    (ProportionalWidth / finalSize.Width);

                contentSize.Height = ProportionalHeight * prop;
                contentSize.Width = finalSize.Width;
            }
            else
            {
                var prop = finalSize.Height < ProportionalHeight ?
                    (finalSize.Height / ProportionalHeight) :
                    (ProportionalHeight / finalSize.Height);

                contentSize.Width = ProportionalWidth * prop;
                contentSize.Height = finalSize.Height;
            }

            
            

            return base.ArrangeOverride(contentSize);
        }

    }

    public enum ResizeMode
    {
        Horizontal,
        Vertical
    }
}
