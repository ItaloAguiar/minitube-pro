﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media.Core;
using Windows.Media.Playback;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// O modelo de item de Página em Branco está documentado em https://go.microsoft.com/fwlink/?LinkId=234238

namespace MiniTube
{
    /// <summary>
    /// Uma página vazia que pode ser usada isoladamente ou navegada dentro de um Quadro.
    /// </summary>
    public sealed partial class CompactPlayerView : Page
    {
        public CompactPlayerView()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var m = e.Parameter as MediaPlayer;

            var pbs = m.PlaybackSession;

            player.SetMediaPlayer(new MediaPlayer());
            player.MediaPlayer.Source = MediaSource.CreateFromUri(((MediaSource)m.Source).Uri);
            player.MediaPlayer.PlaybackSession.Position = pbs.Position;
            player.MediaPlayer.Play();
            
            

            //player.Source = m.Source;
            //player.MediaPlayer.PlaybackSession.Position = pbs.Position;
            //player.MediaPlayer.Play();

            base.OnNavigatedTo(e);
        }

        public TimeSpan Position
        {
            get { return player.MediaPlayer.PlaybackSession.Position; }
        }

        public void Dispose()
        {
            player.MediaPlayer.Dispose();
        }
    }
}
