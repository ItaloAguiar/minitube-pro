﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.Web.Http;
using System.Text;
using System.Threading.Tasks;
using Windows.Media.Core;
using Windows.Media.MediaProperties;
using Windows.Storage.Streams;

namespace YouTube.UI
{
    internal class YouTubeVideoStream
    {
        public YouTubeVideoStream(Format fmt)
        {
            FMT = fmt;

            VEP = VideoEncodingProperties.CreateH264();
            VEP.Bitrate = fmt.Bitrate;
            VEP.Width = fmt.Width;
            VEP.Height = fmt.Height;

            VSD = new VideoStreamDescriptor(VEP);
            Source = new MediaStreamSource(VSD);
            Source.CanSeek = true;
            Source.Duration = TimeSpan.FromMilliseconds(fmt.ApproxDurationMs);

            Source.Starting += StreamStarting;
            Source.SampleRequested += StreamSampleRequested;
            Source.Closed += StreamClosed;

            timeOffset = new TimeSpan(0);
            byteOffset = 0;
            sampleSize = (uint)(fmt.ContentLength / 40);
            sampleDuration = TimeSpan.FromMilliseconds(fmt.ApproxDurationMs)/40;
        }

        private VideoStreamDescriptor VSD;
        private VideoEncodingProperties VEP;
        private Format FMT;
        private UInt64 byteOffset;
        private uint sampleSize;
        private TimeSpan timeOffset;
        private TimeSpan sampleDuration;

        HttpClient http = new HttpClient();



        public MediaStreamSource Source { get; private set; }


        private void StreamStarting(MediaStreamSource sender, MediaStreamSourceStartingEventArgs args)
        {
            MediaStreamSourceStartingRequest request = args.Request;
            if ((request.StartPosition != null) && request.StartPosition.Value <= Source.Duration)
            {
                UInt64 sampleOffset = (UInt64)request.StartPosition.Value.Ticks / (UInt64)sampleDuration.Ticks;
                timeOffset = new TimeSpan((long)sampleOffset * sampleDuration.Ticks);
                byteOffset = sampleOffset * sampleSize;
            }

        }

        private async void StreamSampleRequested(MediaStreamSource sender, MediaStreamSourceSampleRequestedEventArgs args)
        {
            MediaStreamSourceSampleRequest request = args.Request;


            if (byteOffset + sampleSize <= FMT.ContentLength)
            {
                MediaStreamSourceSampleRequestDeferral deferal = request.GetDeferral();


                // create the MediaStreamSample and assign to the request object.  
                // You could also create the MediaStreamSample using createFromBuffer(...) 

                HttpRequestMessage msg = new HttpRequestMessage(HttpMethod.Get, FMT.Url);
                msg.Headers.Add("Range", $"{byteOffset},{byteOffset + sampleSize}");


                HttpResponseMessage response = await http.SendRequestAsync(msg);
                var stream = await response.Content.ReadAsInputStreamAsync();




                MediaStreamSample sample = await MediaStreamSample.CreateFromStreamAsync(stream, sampleSize, timeOffset);
                sample.Duration = sampleDuration;
                sample.KeyFrame = true;

                // increment the time and byte offset 

                byteOffset += sampleSize;
                timeOffset = timeOffset.Add(sampleDuration);
                request.Sample = sample;
                deferal.Complete();
            }



            //response.Content.
        }

        private void StreamClosed(MediaStreamSource sender, MediaStreamSourceClosedEventArgs args)
        {

            sender.SampleRequested -= StreamSampleRequested;
            sender.Starting -= StreamStarting;
            sender.Closed -= StreamClosed;

            if (sender == Source)
            {
                Source = null;
            }
        }
    }
}
