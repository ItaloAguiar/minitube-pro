﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace YouTube.UI
{
    [DataContract]
    internal class LiveStreamabilityRenderer
    {
        [DataMember(Name = "videoId")]
        public string videoId { get; set; }

        [DataMember(Name = "broadcastId")]
        public string BroadcastId { get; set; }

        [DataMember(Name = "pollDelayMs")]
        public string PollDelayMs { get; set; }
    }

    [DataContract]
    internal class Format
    {
        [DataMember(Name = "itag")]
        public int ITag { get; set; }

        [DataMember(Name = "url")]
        public Uri Url { get; set; }

        [DataMember(Name = "mimeType")]
        public string MimeType { get; set; }

        [DataMember(Name = "bitrate")]
        public uint Bitrate { get; set; }

        [DataMember(Name = "width")]
        public uint Width { get; set; }

        [DataMember(Name = "height")]
        public uint Height { get; set; }

        [DataMember(Name = "lastModified")]
        public string LastModified { get; set; }

        [DataMember(Name = "quality")]
        public string Quality { get; set; }

        [DataMember(Name = "fps")]
        public int Fps { get; set; }

        [DataMember(Name = "qualityLabel")]
        public string QualityLabel { get; set; }

        [DataMember(Name = "projectionType")]
        public string ProjectionType { get; set; }

        [DataMember(Name = "targetDurationSec")]
        public double TargetDurationSec { get; set; }

        [DataMember(Name = "maxDvrDurationSec")]
        public double MaxDvrDurationSec { get; set; }

        [DataMember(Name = "highReplication")]
        public bool? HighReplication { get; set; }

        [DataMember(Name = "audioQuality")]
        public string AudioQuality { get; set; }

        [DataMember(Name = "audioSampleRate")]
        public string AudioSampleRate { get; set; }

        [DataMember(Name = "contentLength")]
        public UInt64 ContentLength { get; set; }

        [DataMember(Name = "averageBitrate")]
        public int AverageBitrate { get; set; }

        [DataMember(Name = "approxDurationMs")]
        public uint ApproxDurationMs { get; set; }
    }

    [DataContract]
    internal class Range
    {
        [DataMember(Name = "start")]
        public string Start { get; set; }

        [DataMember(Name = "end")]
        public string End { get; set; }
    }

    [DataContract]
    internal class ColorInfo
    {
        [DataMember(Name = "primaries")]
        public string Primaries { get; set; }

        [DataMember(Name = "transferCharacteristics")]
        public string TransferCharacteristics { get; set; }

        [DataMember(Name = "matrixCoefficients")]
        public string MatrixCoefficients { get; set; }
    }

    [DataContract]
    internal class LiveStreamability
    {
        [DataMember(Name = "liveStreamabilityRenderer")]
        public LiveStreamabilityRenderer LiveStreamabilityRenderer { get; set; }
    }

    [DataContract]
    internal class PlayabilityStatus
    {
        [DataMember(Name = "status")]
        public string Status { get; set; }

        [DataMember(Name = "liveStreamability")]
        public LiveStreamability LiveStreamability { get; set; }
    }

    [DataContract]
    internal class AdaptiveFormat
    {
        [DataMember(Name = "itag")]
        public int ITag { get; set; }

        [DataMember(Name = "url")]
        public Uri Url { get; set; }

        [DataMember(Name = "mimeType")]
        public string MimeType { get; set; }

        [DataMember(Name = "bitrate")]
        public int Bitrate { get; set; }

        [DataMember(Name = "width")]
        public int Width { get; set; }

        [DataMember(Name = "height")]
        public int Height { get; set; }

        [DataMember(Name = "lastModified")]
        public string LastModified { get; set; }

        [DataMember(Name = "quality")]
        public string Quality { get; set; }

        [DataMember(Name = "fps")]
        public int Fps { get; set; }

        [DataMember(Name = "qualityLabel")]
        public string QualityLabel { get; set; }

        [DataMember(Name = "projectionType")]
        public string ProjectionType { get; set; }

        [DataMember(Name = "targetDurationSec")]
        public double TargetDurationSec { get; set; }

        [DataMember(Name = "maxDvrDurationSec")]
        public double MaxDvrDurationSec { get; set; }

        [DataMember(Name = "highReplication")]
        public bool? HighReplication { get; set; }

        [DataMember(Name = "audioQuality")]
        public string AudioQuality { get; set; }

        [DataMember(Name = "audioSampleRate")]
        public string AudioSampleRate { get; set; }

        [DataMember(Name = "initRange ")]
        public Range InitRange { get; set; }

        [DataMember(Name = "indexRange")]
        public Range IndexRange { get; set; }

        [DataMember(Name = "contentLength")]
        public string ContentLength { get; set; }

        [DataMember(Name = "approxDurationMs")]
        public string ApproxDurationMs { get; set; }

        [DataMember(Name = "colorInfo")]
        public ColorInfo ColorInfo { get; set; }

    }

    [DataContract]
    internal class StreamingData
    {
        [DataMember(Name = "expiresInSeconds")]
        public string ExpiresInSeconds { get; set; }

        [DataMember(Name = "adaptiveFormats")]
        public List<AdaptiveFormat> AdaptiveFormats { get; set; }

        [DataMember(Name = "dashManifestUrl")]
        public string DashManifestUrl { get; set; }

        [DataMember(Name = "hlsManifestUrl")]
        public string HlsManifestUrl { get; set; }

        [DataMember(Name = "formats")]
        public List<Format> Formats { get; set; }
    }

    [DataContract]
    internal class BaseUrl
    {
        [DataMember(Name = "baseUrl")]
        public Uri Url { get; set; }
    }

    [DataContract]
    internal class RequestUrl : BaseUrl
    {
        [DataMember(Name = "elapsedMediaTimeSeconds")]
        public int ElapsedMediaTimeSeconds { get; set; }
    }

    [DataContract]
    internal class PlaybackTracking
    {
        [DataMember(Name = "videostatsPlaybackUrl")]
        public BaseUrl VideostatsPlaybackUrl { get; set; }

        [DataMember(Name = "videostatsDelayplayUrl")]
        public RequestUrl VideostatsDelayplayUrl { get; set; }

        [DataMember(Name = "videostatsWatchtimeUrl")]
        public BaseUrl VideostatsWatchtimeUrl { get; set; }

        [DataMember(Name = "ptrackingUrl")]
        public BaseUrl PTrackingUrl { get; set; }

        [DataMember(Name = "qoeUrl")]
        public BaseUrl QoeUrl { get; set; }

        [DataMember(Name = "setAwesomeUrl")]
        public RequestUrl SetAwesomeUrl { get; set; }

        [DataMember(Name = "atrUrl")]
        public RequestUrl AtrUrl { get; set; }

        [DataMember(Name = "youtubeRemarketingUrl")]
        public RequestUrl YoutubeRemarketingUrl { get; set; }

    }

    [DataContract]
    internal class Thumbnail
    {
        [DataMember(Name = "url")]
        public Uri Url { get; set; }

        [DataMember(Name = "width")]
        public int Width { get; set; }

        [DataMember(Name = "height")]
        public int Height { get; set; }
    }


    [DataContract]
    internal class VideoDetails
    {
        [DataMember(Name = "videoId")]
        public string VideoId { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "lengthSeconds")]
        public string LengthSeconds { get; set; }

        [DataMember(Name = "isLive")]
        public bool IsLive { get; set; }

        [DataMember(Name = "keywords")]
        public List<string> Keywords { get; set; }

        [DataMember(Name = "channelId")]
        public string ChannelId { get; set; }

        [DataMember(Name = "isCrawlable")]
        public bool IsCrawlable { get; set; }

        [DataMember(Name = "thumbnail.thumbnails")]
        public List<Thumbnail> Thumbnails { get; set; }

        [DataMember(Name = "viewCount")]
        public string ViewCount { get; set; }

        [DataMember(Name = "author")]
        public string Author { get; set; }

        [DataMember(Name = "isLiveContent")]
        public bool IsLiveContent { get; set; }


    }

    [DataContract]
    internal class LivePlayerConfig
    {
        [DataMember(Name = "liveReadaheadSeconds")]
        public double LiveReadaheadSeconds { get; set; }
    }

    [DataContract]
    internal class PlayerConfig
    {
        [DataMember(Name = "livePlayerConfig")]
        public LivePlayerConfig LivePlayerConfig { get; set; }

        [DataMember(Name = "audioConfig")]
        public AudioConfig AudioConfig { get; set; }

        [DataMember(Name = "streamSelectionConfig")]
        public StreamSelectionConfig StreamSelectionConfig { get; set; }
    }

    [DataContract]
    internal class PlayerStoryboardSpecRenderer
    {
        [DataMember(Name = "spec")]
        public string Spec { get; set; }
    }

    [DataContract]
    internal class Storyboards
    {
        [DataMember(Name = "playerLiveStoryboardSpecRenderer")]
        public PlayerStoryboardSpecRenderer PlayerLiveStoryboardSpecRenderer { get; set; }

        [DataMember(Name = "playerStoryboardSpecRenderer")]
        public PlayerStoryboardSpecRenderer PlayerStoryboardSpecRenderer { get; set; }
    }

    [DataContract]
    internal class PlayerAttestationRenderer
    {
        [DataMember(Name = "challenge")]
        public string Challenge { get; set; }
    }

    [DataContract]
    internal class Attestation
    {
        [DataMember(Name = "playerAttestationRenderer")]
        public PlayerAttestationRenderer PlayerAttestationRenderer { get; set; }
    }

    [DataContract]
    internal class Run
    {
        [DataMember(Name = "text")]
        public string Text { get; set; }
    }

    [DataContract]
    internal class Text
    {
        [DataMember(Name = "runs")]
        public List<Run> Runs { get; set; }
    }

    [DataContract]
    internal class BrowseEndpoint
    {
        [DataMember(Name = "browseId")]
        public string BrowseId { get; set; }

        [DataMember(Name = "params")]
        public string Params { get; set; }
    }

    [DataContract]
    internal class NavigationEndpoint
    {
        [DataMember(Name = "clickTrackingParams")]
        public string ClickTrackingParams { get; set; }

        [DataMember(Name = "browseEndpoint")]
        public BrowseEndpoint BrowseEndpoint { get; set; }
    }

    [DataContract]
    internal class ButtonRenderer
    {
        [DataMember(Name = "style")]
        public string Style { get; set; }

        [DataMember(Name = "size")]
        public string Size { get; set; }

        [DataMember(Name = "text")]
        public Text Text { get; set; }

        [DataMember(Name = "navigationEndpoint")]
        public NavigationEndpoint NavigationEndpoint { get; set; }

        [DataMember(Name = "trackingParams")]
        public string TrackingParams { get; set; }
    }

    [DataContract]
    internal class DataButton
    {
        [DataMember(Name = "buttonRenderer")]
        public ButtonRenderer ButtonRenderer { get; set; }
    }

    [DataContract]
    internal class UiActions
    {
        [DataMember(Name = "hideEnclosingContainer")]
        public bool HideEnclosingContainer { get; set; }
    }

    [DataContract]
    internal class FeedbackEndpoint
    {
        [DataMember(Name = "feedbackToken")]
        public string FeedbackToken { get; set; }

        [DataMember(Name = "uiActions")]
        public UiActions UIActions { get; set; }
    }

    [DataContract]
    internal class Endpoint
    {
        [DataMember(Name = "clickTrackingParams ")]
        public string ClickTrackingParams { get; set; }

        [DataMember(Name = "feedbackEndpoint")]
        public FeedbackEndpoint FeedbackEndpoint { get; set; }
    }

    [DataContract]
    internal class ButtonRenderer2
    {
        [DataMember(Name = "style")]
        public string Style { get; set; }

        [DataMember(Name = "size")]
        public string Size { get; set; }

        [DataMember(Name = "text")]
        public Text Text { get; set; }

        [DataMember(Name = "serviceEndpoint")]
        public Endpoint ServiceEndpoint { get; set; }

        [DataMember(Name = "trackingParams")]
        public string TrackingParams { get; set; }
    }

    [DataContract]
    internal class MealbarPromoRenderer
    {
        [DataMember(Name = "messageTexts")]
        public List<Text> MessageTexts { get; set; }

        [DataMember(Name = "actionButton")]
        public DataButton ActionButton { get; set; }

        [DataMember(Name = "dismissButton")]
        public DataButton DismissButton { get; set; }

        [DataMember(Name = "triggerCondition")]
        public string TriggerCondition { get; set; }

        [DataMember(Name = "style")]
        public string Style { get; set; }

        [DataMember(Name = "trackingParams")]
        public string TrackingParams { get; set; }

        [DataMember(Name = "impressionEndpoints")]
        public List<Endpoint> ImpressionEndpoints { get; set; }

        [DataMember(Name = "isVisible")]
        public bool IsVisible { get; set; }

        [DataMember(Name = "messageTitle")]
        public Text MessageTitle { get; set; }
    }

    [DataContract]
    internal class Message
    {
        [DataMember(Name = "mealbarPromoRenderer")]
        public MealbarPromoRenderer MealbarPromoRenderer { get; set; }
    }

    [DataContract]
    internal class AdSafetyReason
    {
        [DataMember(Name = "isEmbed")]
        public bool IsEmbed { get; set; }

        [DataMember(Name = "isRemarketingEnabled")]
        public bool IsRemarketingEnabled { get; set; }

        [DataMember(Name = "isFocEnabled")]
        public bool IsFocEnabled { get; set; }
    }

    [DataContract]
    internal class CaptionTrack
    {
        [DataMember(Name = "baseUrl")]
        public Uri BaseUrl { get; set; }

        [DataMember(Name = "name.simpleText")]
        public string Name { get; set; }

        [DataMember(Name = "vssId")]
        public string VssId { get; set; }

        [DataMember(Name = "languageCode")]
        public string LanguageCode { get; set; }

        [DataMember(Name = "kind")]
        public string Kind { get; set; }

        [DataMember(Name = "isTranslatable")]
        public bool IsTranslatable { get; set; }
    }

    [DataContract]
    internal class AudioTrack
    {
        [DataMember(Name = "captionTrackIndices")]
        public List<int> CaptionTrackIndices { get; set; }

        [DataMember(Name = "visibility")]
        public string Visibility { get; set; }
    }

    [DataContract]
    internal class TranslationLanguage
    {
        [DataMember(Name = "languageCode")]
        public string LanguageCode { get; set; }

        [DataMember(Name = "languageName.simpleText")]
        public string LanguageName { get; set; }
    }

    [DataContract]
    internal class PlayerCaptionsTracklistRenderer
    {
        [DataMember(Name = "captionTracks")]
        public List<CaptionTrack> CaptionTracks { get; set; }

        [DataMember(Name = "audioTracks")]
        public List<AudioTrack> AudioTracks { get; set; }

        [DataMember(Name = "translationLanguages")]
        public List<TranslationLanguage> TranslationLanguages { get; set; }

        [DataMember(Name = "defaultAudioTrackIndex")]
        public int DefaultAudioTrackIndex { get; set; }
    }

    [DataContract]
    internal class Captions
    {
        [DataMember(Name = "playerCaptionsTracklistRenderer")]
        public PlayerCaptionsTracklistRenderer PlayerCaptionsTracklistRenderer { get; set; }
    }

    [DataContract]
    internal class AudioConfig
    {
        [DataMember(Name = "loudnessDb")]
        public double LoudnessDb { get; set; }

        [DataMember(Name = "perceptualLoudnessDb")]
        public double PerceptualLoudnessDb { get; set; }
    }

    [DataContract]
    internal class StreamSelectionConfig
    {
        [DataMember(Name = "maxBitrate")]
        public string MaxBitrate { get; set; }
    }

    [DataContract]
    internal class PlayerData
    {
        [DataMember(Name = "playabilityStatus")]
        public PlayabilityStatus PlayabilityStatus { get; set; }

        [DataMember(Name = "streamingData")]
        public StreamingData StreamingData { get; set; }

        [DataMember(Name = "playbackTracking")]
        public PlaybackTracking PlaybackTracking { get; set; }

        [DataMember(Name = "videoDetails")]
        public VideoDetails VideoDetails { get; set; }

        [DataMember(Name = "playerConfig")]
        public PlayerConfig PlayerConfig { get; set; }

        [DataMember(Name = "storyboards")]
        public Storyboards Storyboards { get; set; }

        [DataMember(Name = "attestation")]
        public Attestation Attestation { get; set; }

        [DataMember(Name = "messages")]
        public List<Message> Messages { get; set; }

        [DataMember(Name = "adSafetyReason")]
        public AdSafetyReason AdSafetyReason { get; set; }

        [DataMember(Name = "captions")]
        public Captions Captions { get; set; }


        public static async Task<PlayerData> FromVideoId(string id)
        {
            string u = $"http://www.youtube.com/get_video_info?&video_id={id}";
            string r = await FromUri(new Uri(u));
            Dictionary<string, string> dic = ParsePlayerData(r);

            if (dic.ContainsKey("player_response"))
            {
                string json = WebUtility.UrlDecode(dic["player_response"]).Replace(@"\u0026", "&");

                return ParseJson<PlayerData>(json);
            }
            else if (dic.ContainsKey("url_encoded_fmt_stream_map"))
            {
                throw new NotImplementedException();
            }
            else if (dic.ContainsKey("adaptive_fmts"))
            {
                throw new NotImplementedException();
            }
            else
                throw new SerializationException();

        }

        private async static Task<string> FromUri(Uri uri)
        {
            HttpClient cli = new HttpClient();
            return await cli.GetStringAsync(uri);
        }

        private static Dictionary<string,string> ParsePlayerData(string data)
        {
            Regex rx = new Regex(@"([\w]+)=([\w\-\%\.\+]*)&?");
            var r = rx.Split(data);

            Dictionary<string, string> dic = new Dictionary<string, string>();

            for(int i = 1; i< r.Length; i = i+3)
            {
                string a = r[i];
                string b = r[i + 1];

                Console.WriteLine($"{a}\t:\t{b}");

                dic.Add(r[i], WebUtility.UrlDecode(r[i + 1]));
            }

            return dic;
        }

        private static T ParseJson<T>(string content)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(content)))
            {
                return (T)ser.ReadObject(ms);
            }
        }
    }
}
