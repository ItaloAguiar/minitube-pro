﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

namespace YouTube.UI
{
    public class Thumb:Control
    {
        public Thumb()
        {
            DefaultStyleKey = typeof(Thumb);
        }

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
        }

        protected override void OnPointerEntered(PointerRoutedEventArgs e)
        {
            base.OnPointerEntered(e);

            VisualStateManager.GoToState(this, "PointerOver", true);
        }

        protected override void OnPointerExited(PointerRoutedEventArgs e)
        {
            base.OnPointerExited(e);

            VisualStateManager.GoToState(this, "Normal", true);
        }

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(ImageSource), typeof(Thumb), null);

        public ImageSource Source
        {
            get
            {
                return (ImageSource)this.GetValue(SourceProperty);
            }
            set
            {
                this.SetValue(SourceProperty, value);
            }
        }

        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register("Title", typeof(string), typeof(Thumb), null);

        public string Title
        {
            get
            {
                return (string)this.GetValue(TitleProperty);
            }
            set
            {
                this.SetValue(TitleProperty, value);
            }
        }

        public static readonly DependencyProperty AuthorProperty =
            DependencyProperty.Register("Author", typeof(string), typeof(Thumb), null);

        public string Author
        {
            get
            {
                return (string)this.GetValue(AuthorProperty);
            }
            set
            {
                this.SetValue(AuthorProperty, value);
            }
        }

        public static readonly DependencyProperty ViewCountProperty =
            DependencyProperty.Register("ViewCount", typeof(string), typeof(Thumb), new PropertyMetadata(null));

        public string ViewCount
        {
            get
            {
                return (string)this.GetValue(ViewCountProperty);
            }
            set
            {
                this.SetValue(ViewCountProperty, value);
            }
        }

        public static readonly DependencyProperty DurationProperty =
            DependencyProperty.Register("Duration", typeof(string), typeof(Thumb), null);

        public string Duration
        {
            get
            {
                return (string)this.GetValue(DurationProperty);
            }
            set
            {
                this.SetValue(DurationProperty, value);
            }
        }
    }
}
