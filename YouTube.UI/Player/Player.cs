﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using Windows.Foundation.Metadata;
using Windows.Media.Core;
using Windows.Media.Playback;
using Windows.System.Display;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;

// The Templated Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234235

namespace YouTube.UI
{

    [MarshalingBehavior(MarshalingType.Agile)]
    [WebHostHidden()]
    [TemplatePart(Name = "PART_PLAYER", Type =typeof(MediaPlayerElement))]
    public class Player : Control
    {
        #region Constructor
        public Player()
        {
            this.DefaultStyleKey = typeof(Player);

            mPlayer = new MediaPlayer();
            YouTubeTransportControl = new YouTubePlayerTransportControls();
        }
        #endregion

        #region Fields
        protected MediaPlayerElement player;
        protected MediaPlayer mPlayer;
        protected YouTubePlayerTransportControls YouTubeTransportControl;
        
        #endregion

        #region Player Setup
        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            

            player = GetTemplateChild("PART_PLAYER") as MediaPlayerElement;
            if(player != null)
            {
                player.TransportControls = YouTubeTransportControl;
                player.SetMediaPlayer(mPlayer);
                YouTubeTransportControl.SetPlayer(player);
            }
        }


        #endregion

        #region Properties

        protected static readonly DependencyProperty VideoProperty =
            DependencyProperty.Register("Video", typeof(API.DataModel.Videos.Item), typeof(Player), 
                new PropertyMetadata(null, new PropertyChangedCallback(OnVideoChanged)));

        private static void OnVideoChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((Player)d).SetVideo(e.NewValue as API.DataModel.Videos.Item);
        }

        public API.DataModel.Videos.Item Video
        {
            get
            {
                return (API.DataModel.Videos.Item)this.GetValue(VideoProperty);
            }
            set
            {
                this.SetValue(VideoProperty, value);
            }
        }

        protected static readonly DependencyProperty RelatedVideosProperty =
            DependencyProperty.Register("RelatedVideos", typeof(API.DataModel.Videos.Item[]), typeof(Player),
                new PropertyMetadata(null, new PropertyChangedCallback(OnRelatedVideosChanged)));

        private static void OnRelatedVideosChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((Player)d).SetRelatedVideos(e.NewValue as API.DataModel.Videos.Item[]);
        }

        public API.DataModel.Videos.Item[] RelatedVideos
        {
            get
            {
                return (API.DataModel.Videos.Item[])this.GetValue(RelatedVideosProperty);
            }
            set
            {
                this.SetValue(RelatedVideosProperty, value);
            }
        }


        protected static readonly DependencyProperty ErrorMessageProperty =
            DependencyProperty.Register("ErrorMessage", typeof(string), typeof(Player), null);
        
        public string ErrorMessage
        {
            get
            {
                return (string)this.GetValue(ErrorMessageProperty);
            }
            set
            {
                this.SetValue(ErrorMessageProperty, value);
            }
        }

       
        #endregion

        private void SetVideoStreams(IEnumerable<Format> s)
        {
            if(YouTubeTransportControl != null)
            {
                YouTubeTransportControl.MediaStreams = s;
            }
        }

        private void SetRelatedVideos(API.DataModel.Videos.Item[] items)
        {
            if (YouTubeTransportControl != null)
            {
                YouTubeTransportControl.RelatedVideos = items;
            }
        }

        private void SetVideo(API.DataModel.Videos.Item video)
        {
            if (YouTubeTransportControl != null)
            {
                YouTubeTransportControl.Video = video;
            }
        }

        public async void SetVideo(string videoId)
        {
            var data = await PlayerData.FromVideoId(videoId);
            SetVideoStreams(data.StreamingData.Formats);
        }

        #region Player's Logic

        #endregion


    }
}
