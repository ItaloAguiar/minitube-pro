﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Shapes;

// The Templated Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234235

namespace YouTube.UI
{
    [TemplatePart(Name="progressPath", Type=typeof(Path))]
    public class CircularProgressbar : Control
    {
        public CircularProgressbar()
        {
            this.DefaultStyleKey = typeof(CircularProgressbar);
        }
        protected Path progressPath;

        protected override void OnApplyTemplate()
        {
            progressPath = GetTemplateChild("progressPath") as Path;
        }
        

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(CircularProgressbar), new PropertyMetadata(0.0, onValueChanged));

        private static void onValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (CircularProgressbar)d;
            var value = Math.Truncate((double)e.NewValue) / 100;
            if (value > 1) value = 1; else if (value < 0) value = 0;
            if(control.progressPath != null)
            control.drawSector(value);            
        }
        /// <summary>
        /// Gets or Sets the IsFullScreen Property
        /// </summary>
        public double Value
        {
            get
            {
                return (double)this.GetValue(ValueProperty);
            }
            set
            {
                this.SetValue(ValueProperty, value); 
                
            }
        }


        public static readonly DependencyProperty FillProperty =
            DependencyProperty.Register("Fill", typeof(Brush), typeof(CircularProgressbar), new PropertyMetadata(new SolidColorBrush(Colors.Black), onFillChanged));

        private static void onFillChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (CircularProgressbar)d;

        }
        /// <summary>
        /// Gets or Sets the IsFullScreen Property
        /// </summary>
        public Brush Fill
        {
            get
            {
                return (Brush)this.GetValue(FillProperty);
            }
            set
            {
                this.SetValue(FillProperty, value);
            }
        }

        public static readonly DependencyProperty StrokeProperty =
            DependencyProperty.Register("Stroke", typeof(Brush), typeof(CircularProgressbar), new PropertyMetadata(new SolidColorBrush(Colors.Black), onStrokeChanged));

        private static void onStrokeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (CircularProgressbar)d;

        }
        /// <summary>
        /// Gets or Sets the IsFullScreen Property
        /// </summary>
        public Brush Stroke
        {
            get
            {
                return (Brush)this.GetValue(StrokeProperty);
            }
            set
            {
                this.SetValue(StrokeProperty, value);
            }
        }

        public static readonly DependencyProperty StrokeThicknessProperty =
            DependencyProperty.Register("StrokeThickness", typeof(double), typeof(CircularProgressbar), new PropertyMetadata(4.0, onStrokeThicknessChanged));

        private static void onStrokeThicknessChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (CircularProgressbar)d;

        }
        /// <summary>
        /// Gets or Sets the IsFullScreen Property
        /// </summary>
        public double StrokeThickness
        {
            get
            {
                return (double)this.GetValue(StrokeProperty);
            }
            set
            {
                this.SetValue(StrokeProperty, value);
            }
        }
        public static readonly DependencyProperty MaximumProperty =
            DependencyProperty.Register("Maximum", typeof(double), typeof(CircularProgressbar), new PropertyMetadata(100.00, onMaximumChanged));

        private static void onMaximumChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (CircularProgressbar)d;

        }
        /// <summary>
        /// Gets or Sets the IsFullScreen Property
        /// </summary>
        public double Maximum
        {
            get
            {
                var value = this.GetValue(MaximumProperty);
                return (double)value;
            }
            set
            {
                this.SetValue(MaximumProperty, value);
            }
        }
        private void drawSector(double value)
        {

            /*
             * The circular pie consists of three parts
             * a. the background
             * b. the pie / sector
             * c. the hole
             * 
             * The path object in the user control will be used to
             * create the pie/sector. Ellipses are used to create the other
             * two. This method draws a sector of a circle based on the value
             * passed into this function.
             * 
             * A sector has three parts -
             * a. a line segment from center to circumfrence
             * b. Arcs - parts of the circumfrence
             * c. a line segment from circumfrence to center
             */

            //Clean the path
            progressPath.SetValue(Path.DataProperty, null);
            PathGeometry pg = new PathGeometry();
            PathFigure fig = new PathFigure();

            //if the value is zero, do nothing further
            if (value == 0) return;

            //a few variables for good measure
            double height = this.ActualHeight;
            double width = this.ActualWidth;
            double radius = height / 2;
            double theta = (360 * value) - 90;  // <--- the coordinate system is flipped with 0,0 at top left. Hence the -90
            double xC = radius;
            double yC = radius;

            //this is to ensure that the fill is complete, otherwise a slight
            //gap is left in the fill of the sector. By incrementing it by one
            //we fill that gap as the angle is now 361 deg (-90 for the coord sys inversion)
            if (value == 1) theta += 1;

            //finalPoint represents the point along the circumfrence after
            //which the sector ends and the line back to the center begins
            //use parametric equations for a circule to figure that out
            // <--- PI / 180 = 0.0174

            double finalPointX = xC + (radius * Math.Cos(theta * 0.0174));
            double finalPointY = yC + (radius * Math.Sin(theta * 0.0174));

            //Now we have calculated all the points we need to start drawing the
            //segments for the figure. Drawing a figure in WPF is like using a pencil
            //without lifting the tip. Each segment specifies an end point and
            //is connected to the previous segment's end point. 

            fig.StartPoint = new Point(radius, radius);                                         //start by placing the pencil's tip at the center of the circle
            LineSegment firstLine = new LineSegment();                //the first line segment goes vertically upwards (radius,0)
            firstLine.Point = new Point(radius, 0);
            fig.Segments.Add(firstLine);                                                        //draw that line segment

            //Now we have to draw the arc for this sector. The way drawing works in wpf,
            //in order to get a proper and coherent circumfrence, we need to draw separate
            //arcs everytime the sector exceeds a quarter of the circle. 

            if (value > 0.25)
            {                                                                 //if the sector will exceed the first quarter
                ArcSegment firstQuart = new ArcSegment();                                       //first get ready to draw the first quarter's arc
                firstQuart.Point = new Point(width, radius);                                    //being a quarter, it ends at (width,radius)
                firstQuart.SweepDirection = SweepDirection.Clockwise;                           //we're drawing clockwise
                //firstQuart.IsLargeArc = true;                                                    //yes, it is stroked
                firstQuart.Size = new Size(radius, radius);                                     //the size of this is for a circle with r = radius
                fig.Segments.Add(firstQuart);                                                   //now draw this arc (fill property would make it a sector)
            }

            if (value > 0.5)
            {                                                                  //if the secotr will exceed the second quarter
                ArcSegment secondQuart = new ArcSegment();                                      //get ready to draw the second quarter's arc too
                secondQuart.Point = new Point(radius, height);                                  //..
                secondQuart.SweepDirection = SweepDirection.Clockwise;                          //..
                //secondQuart.IsLargeArc= true;                                                   //..
                secondQuart.Size = new Size(radius, radius);                                    //..
                fig.Segments.Add(secondQuart);                                                  //draw the second quarter's arc
            }

            if (value > 0.75)
            {                                                                 //Similarly if the sector will exceed three quarters
                ArcSegment thirdQuart = new ArcSegment();                                       //get ready to draw the third quarter's arc (starts at the end of the previous quarter)
                thirdQuart.Point = new Point(0, radius);                                        //ends at (0,radius) 
                thirdQuart.SweepDirection = SweepDirection.Clockwise;
                //thirdQuart.IsLargeArc= true;
                thirdQuart.Size = new Size(radius, radius);
                fig.Segments.Add(thirdQuart);                                                   //draw this arc
            }

            ArcSegment finalQuart = new ArcSegment();                                           //finally, once we're done with the quarter arcs
            finalQuart.Point = new Point(finalPointX, finalPointY);                             //let's get ready to draw the last arc/sector
            finalQuart.SweepDirection = SweepDirection.Clockwise;                               //which will complete the remaining circumfrence and end
            //finalQuart.IsLargeArc = true;                                                       // at (finalPointX, finalPointY)
            finalQuart.Size = new Size(radius, radius);
            fig.Segments.Add(finalQuart);                                                       //draw this arc

            //Now one line segment, arcs for the circumfrence - all that is done
            //let's draw a line segment from the end of the sector/arcs back to the
            //center of the circle

            LineSegment lastLine = new LineSegment();
            lastLine.Point = new Point(radius, radius);
            fig.Segments.Add(lastLine);

            //Now take these figures and add this to the path geometry
            //then add the path geometry to the path's data property
            pg.Figures.Add(fig);
            progressPath.SetValue(Path.DataProperty, pg);
        }
    }
}
