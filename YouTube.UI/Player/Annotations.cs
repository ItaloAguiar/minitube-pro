﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace YouTube.UI
{
    [XmlRoot(ElementName = "annotations")]
    internal class Annotations
    {
        [XmlElement(ElementName = "annotation")]
        internal List<Annotation> Annotation { get; set; }
        [XmlAttribute(AttributeName = "itct")]
        internal string Itct { get; set; }

        internal async static Task<Document> FromString(string content)
        {
            XmlSerializer ser = new XmlSerializer(typeof(Document));
            MemoryStream ms = new MemoryStream();
            StreamWriter sw = new StreamWriter(ms);
            sw.Write(content);
            await sw.FlushAsync();
            sw.Close();
            return (Document)ser.Deserialize(ms);
        }

        internal async static Task<Document> FromUri(Uri uri)
        {
            HttpClient cli = new HttpClient();
            string s = await cli.GetStringAsync(uri);
            return await FromString(s);
        }

        internal async static Task<Document> FromVideoId(string id)
        {
            string u = $"https://www.youtube.com/annotations_invideo?features=0&legacy=1&video_id={id}";
            return await FromUri(new Uri(u));
        }
    }

    [XmlRoot(ElementName = "rectRegion")]
    internal class RectRegion
    {
        [XmlAttribute(AttributeName = "x")]
        internal string X { get; set; }
        [XmlAttribute(AttributeName = "y")]
        internal string Y { get; set; }
        [XmlAttribute(AttributeName = "w")]
        internal string W { get; set; }
        [XmlAttribute(AttributeName = "h")]
        internal string H { get; set; }
        [XmlAttribute(AttributeName = "t")]
        internal string T { get; set; }
    }

    [XmlRoot(ElementName = "movingRegion")]
    internal class MovingRegion
    {
        [XmlElement(ElementName = "rectRegion")]
        internal List<RectRegion> RectRegion { get; set; }
        [XmlAttribute(AttributeName = "type")]
        internal string Type { get; set; }
    }

    [XmlRoot(ElementName = "segment")]
    internal class Segment
    {
        [XmlElement(ElementName = "movingRegion")]
        internal MovingRegion MovingRegion { get; set; }
        [XmlAttribute(AttributeName = "spaceRelative")]
        internal string SpaceRelative { get; set; }
    }

    [XmlRoot(ElementName = "appearance")]
    internal class Appearance
    {
        [XmlAttribute(AttributeName = "bgAlpha")]
        internal string BgAlpha { get; set; }
        [XmlAttribute(AttributeName = "textSize")]
        internal string TextSize { get; set; }
        [XmlAttribute(AttributeName = "highlightFontColor")]
        internal string HighlightFontColor { get; set; }
        [XmlAttribute(AttributeName = "bgColor")]
        internal string BgColor { get; set; }
        [XmlAttribute(AttributeName = "highlightWidth")]
        internal string HighlightWidth { get; set; }
        [XmlAttribute(AttributeName = "borderAlpha")]
        internal string BorderAlpha { get; set; }
    }

    [XmlRoot(ElementName = "condition")]
    internal class Condition
    {
        [XmlAttribute(AttributeName = "ref")]
        internal string Ref { get; set; }
        [XmlAttribute(AttributeName = "state")]
        internal string State { get; set; }
    }

    [XmlRoot(ElementName = "trigger")]
    internal class Trigger
    {
        [XmlElement(ElementName = "condition")]
        internal Condition Condition { get; set; }
    }

    [XmlRoot(ElementName = "annotation")]
    internal class Annotation
    {
        [XmlElement(ElementName = "segment")]
        internal Segment Segment { get; set; }
        [XmlElement(ElementName = "appearance")]
        internal Appearance Appearance { get; set; }
        [XmlElement(ElementName = "trigger")]
        internal Trigger Trigger { get; set; }
        [XmlAttribute(AttributeName = "id")]
        internal string Id { get; set; }
        [XmlAttribute(AttributeName = "type")]
        internal string Type { get; set; }
        [XmlAttribute(AttributeName = "style")]
        internal string Style { get; set; }
        [XmlAttribute(AttributeName = "logable")]
        internal string Logable { get; set; }
        [XmlAttribute(AttributeName = "itct")]
        internal string Itct { get; set; }
        [XmlElement(ElementName = "TEXT")]
        internal string TEXT { get; set; }
        [XmlElement(ElementName = "action")]
        internal Action Action { get; set; }
        [XmlAttribute(AttributeName = "log_data")]
        internal string Log_data { get; set; }
        [XmlElement(ElementName = "data")]
        internal string Data { get; set; }
    }

    [XmlRoot(ElementName = "url")]
    internal class Url
    {
        [XmlAttribute(AttributeName = "target")]
        internal string Target { get; set; }
        [XmlAttribute(AttributeName = "value")]
        internal string Value { get; set; }
        [XmlAttribute(AttributeName = "link_class")]
        internal string Link_class { get; set; }
        [XmlAttribute(AttributeName = "type")]
        internal string Type { get; set; }
    }

    [XmlRoot(ElementName = "action")]
    internal class Action
    {
        [XmlElement(ElementName = "url")]
        internal Url Url { get; set; }
        [XmlAttribute(AttributeName = "type")]
        internal string Type { get; set; }
        [XmlAttribute(AttributeName = "trigger")]
        internal string Trigger { get; set; }
    }

    

    [XmlRoot(ElementName = "document")]
    internal class Document
    {
        [XmlElement(ElementName = "annotations")]
        internal Annotations Annotations { get; set; }
    }

}
