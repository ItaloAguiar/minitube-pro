﻿using Windows.Media.Playback;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using System;
using Windows.UI.Xaml.Input;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.Media.Core;
using System.Net.Http;
using Windows.Media.Streaming.Adaptive;
using Windows.Foundation.Metadata;

namespace YouTube.UI
{
    [TemplatePart(Name = "PART_GRIDVIEW", Type = typeof(GridView))]
    [TemplatePart(Name = "PART_CB_RES", Type = typeof(ComboBox))]
    public class YouTubePlayerTransportControls: MediaTransportControls
    {
        public YouTubePlayerTransportControls()
        {
            this.DefaultStyleKey = typeof(YouTubePlayerTransportControls);
        }

        private MediaPlayerElement MediaPlayer;
        protected TextBlock TotalTime;
        protected Button UpNextButton;
        protected Button SettingsButton;
        protected Button Volume;
        protected Slider VolumeSlider;
        protected Panel VolumePanel;
        protected GridView PART_GRIDVIEW;
        protected ComboBox PART_CB_RES;


        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();


            //MediaPlayer = FindParent<MediaPlayerElement>(this);
            TotalTime = GetTemplateChild("TotalTimeElement") as TextBlock;
            UpNextButton = GetTemplateChild("Next") as Button;
            SettingsButton = GetTemplateChild("Settings") as Button;
            Volume = GetTemplateChild("VolumeMuteButton") as Button;
            VolumeSlider = GetTemplateChild("VolumeSlider") as Slider;
            VolumePanel = GetTemplateChild("VolumePanel") as Panel;
            PART_GRIDVIEW = GetTemplateChild("PART_GRIDVIEW") as GridView;
            PART_CB_RES = GetTemplateChild("PART_CB_RES") as ComboBox;

            VisualStateManager.GoToState(this, "OverlaysHidden", true);

            

            PART_CB_RES.SelectionChanged += (s, a) =>
            {
                var k = PART_CB_RES.SelectedItem as Format;
                // var u = MediaSource.CreateFromUri(k.Source);

                //YouTubeVideoStream ytvs = new YouTubeVideoStream(k.Format);

                //AdaptiveMediaSourceCreationResult result =
                //    await AdaptiveMediaSource.CreateFromUriAsync(k.Source);

                //if (result.Status != AdaptiveMediaSourceCreationStatus.Success)
                //{
                //    // TODO: Handle adaptive media source creation errors.
                //    return;
                //}
                //var mediaSource = MediaSource.CreateFromAdaptiveMediaSource(result.MediaSource);
                //var mediaPlaybackItem = new MediaPlaybackItem(mediaSource);

                //var source = MediaSource.CreateFromIMediaSource(ytvs.Source);
                var source = MediaSource.CreateFromUri(k.Url);

                MediaPlayer.MediaPlayer.Source = source;
                MediaPlayer.MediaPlayer.Play();
            };


            //if (MediaPlayer!= null)
            //{
            //    MediaPlayer.MediaPlayer.MediaOpened += MediaOpened;
            //}

            if(UpNextButton != null)
            {
                UpNextButton.PointerEntered += UpNextPointerOver;
                UpNextButton.PointerExited += UpNextPointerExited;
            }

            if(SettingsButton != null)
            {
                SettingsButton.Click += SettingsClick;
            }

            if(VolumePanel != null)
            {
                VolumePanel.PointerMoved += VolumePointerEntered;
                VolumePanel.PointerEntered += VolumePointerEntered;
                VolumePanel.PointerExited += VolumePointerExited;
            }
            
        }

        private void VolumePointerEntered(object sender, PointerRoutedEventArgs e)
        {
            VisualStateManager.GoToState(this, "VolumeBarVisible", false);
        }

        private void VolumePointerExited(object sender, PointerRoutedEventArgs e)
        {
            VisualStateManager.GoToState(this, "VolumeBarHidden", false);
        }

        private void SettingsClick(object sender, RoutedEventArgs e)
        {
            VisualStateManager.GoToState(this, "SettingsVisible", false);
        }

        private void UpNextPointerExited(object sender, PointerRoutedEventArgs e)
        {
            VisualStateManager.GoToState(this, "NextHidden", false);
        }

        private void UpNextPointerOver(object sender, PointerRoutedEventArgs e)
        {
            VisualStateManager.GoToState(this, "NextVisible", false);
        }

        private async void MediaOpened(MediaPlayer sender, object args)
        {
            if (TotalTime != null)
            {
                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {
                    TotalTime.Text = MediaPlayer.MediaPlayer.PlaybackSession.NaturalDuration.ToString(@"hh\:mm\:ss");
                });
            }
        }

        public static readonly DependencyProperty VideoProperty =
            DependencyProperty.Register("Video", typeof(API.DataModel.Videos.Item), typeof(YouTubePlayerTransportControls),
                new PropertyMetadata(null, new PropertyChangedCallback(OnVideoChanged)));

        private static void OnVideoChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

        public API.DataModel.Videos.Item Video
        {
            get
            {
                return (API.DataModel.Videos.Item)this.GetValue(VideoProperty);
            }
            set
            {
                this.SetValue(VideoProperty, value);
            }
        }

        public static readonly DependencyProperty RelatedVideosProperty =
            DependencyProperty.Register("RelatedVideos", typeof(API.DataModel.Videos.Item[]), typeof(YouTubePlayerTransportControls),
                new PropertyMetadata(null, new PropertyChangedCallback(OnRelatedVideosChanged)));

        private static void OnRelatedVideosChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            API.DataModel.Videos.Item[] items = e.NewValue as API.DataModel.Videos.Item[];
            var gridview = ((YouTubePlayerTransportControls)d).PART_GRIDVIEW;

            if (gridview != null)
            {
                gridview.ItemsSource = items.Take(Math.Min(items.Count(), 12));
            }
        }

        public API.DataModel.Videos.Item[] RelatedVideos
        {
            get
            {
                return (API.DataModel.Videos.Item[])this.GetValue(RelatedVideosProperty);
            }
            set
            {
                this.SetValue(RelatedVideosProperty, value);
            }
        }

        public static readonly DependencyProperty MediaStreamsProperty =
            DependencyProperty.Register("MediaStreams", typeof(IEnumerable<Format>), typeof(YouTubePlayerTransportControls),
                new PropertyMetadata(null, new PropertyChangedCallback(OnMediaStreamsChanged)));

        private static void OnMediaStreamsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            
        }

        internal IEnumerable<Format> MediaStreams
        {
            get
            {
                return (IEnumerable<Format>)this.GetValue(MediaStreamsProperty);
            }
            set
            {
                this.SetValue(MediaStreamsProperty, value);
            }
        }


        internal void SetPlayer(MediaPlayerElement player)
        {
            MediaPlayer = player;

            MediaPlayer.MediaPlayer.MediaEnded += async (s, a) =>
            {
                await Windows.ApplicationModel.Core.CoreApplication.MainView.Dispatcher.RunAsync(
                    Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {
                    VisualStateManager.GoToState(this, "RelatedVideosVisible", true);
                });

            };
        }


        private T FindParent<T>(DependencyObject child) where T : DependencyObject
        {
            var parent = VisualTreeHelper.GetParent(child);

            if (parent is T)
                return (T)parent;
            else if (parent == null)
                return null;
            else
                return FindParent<T>(parent);
        }

    }
}
