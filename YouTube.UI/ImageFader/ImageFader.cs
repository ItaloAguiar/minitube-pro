﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

// The Templated Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234235

namespace YouTube.UI
{
    [TemplateVisualState(Name = "ImageLoaded", GroupName = "ImageStates")]
    public class ImageFader : Control
    {
        public ImageFader()
        {
            this.DefaultStyleKey = typeof(ImageFader);
        }
        protected Image _Image;
        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            _Image = GetTemplateChild("_Image") as Image;
            _Image.ImageOpened += (sender, args) =>
            {
                GoToState("ImageLoaded");
            };
        }
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(ImageSource), typeof(ImageFader), null);
        public ImageSource Source
        {
            get
            {
                return (ImageSource)this.GetValue(SourceProperty);
            }
            set
            {
                this.SetValue(SourceProperty, value);
            }
        }
        private void GoToState(string state)
        {
            try
            {
                VisualStateManager.GoToState(this, state, true);
            }
            catch { }
        }
    }

}
