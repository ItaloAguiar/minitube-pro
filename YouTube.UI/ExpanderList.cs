﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using YouTube.API.DataModel.Comment;

namespace YouTube.UI
{
    [TemplatePart(Name = "PART_TOGGLEBUTTON", Type = typeof(ToggleButton))]
    public class ExpanderList: Control
    {
        public ExpanderList()
        {
            DefaultStyleKey = typeof(ExpanderList);
        }

        protected ToggleButton PART_TOGGLEBUTTON;

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_TOGGLEBUTTON = GetTemplateChild("PART_TOGGLEBUTTON") as ToggleButton;
            if(PART_TOGGLEBUTTON != null)
            {
                PART_TOGGLEBUTTON.Checked += OnChecked;
                PART_TOGGLEBUTTON.Unchecked += OnUnchecked;
            }

            SetVisualState();
        }

        private void OnUnchecked(object sender, RoutedEventArgs e)
        {
            SetItems(false);
        }

        private void OnChecked(object sender, RoutedEventArgs e)
        {
            SetItems(true);
        }

        private void SetItems(bool isOpen)
        {
            IsOpen = isOpen;

            IEnumerable<object> v = ItemsSource as IEnumerable<object>;

            if (v != null)
            {
                if (isOpen)
                {
                    Items = new ObservableCollection<object>(v.Reverse());
                }
                else
                {
                    Items = new ObservableCollection<object>(v.Take(Math.Min(v.Count(), CompactModeItems)).Reverse());
                }
            }
        }

        public static readonly DependencyProperty CompactModeItemsProperty =
            DependencyProperty.Register("CompactModeItems", typeof(int), typeof(ExpanderList), new PropertyMetadata(2));

        public int CompactModeItems
        {
            get
            {
                return (int)this.GetValue(CompactModeItemsProperty);
            }
            set
            {
                this.SetValue(CompactModeItemsProperty, value);
            }
        }

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(object), typeof(ExpanderList), new PropertyMetadata(null, OnItemsSourceChanged));

        public object ItemsSource
        {
            get
            {
                return this.GetValue(ItemsSourceProperty);
            }
            set
            {
                this.SetValue(ItemsSourceProperty, value);
            }
        }

        private static void OnItemsSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ExpanderList l = (ExpanderList)d;
            IEnumerable<object> v =e.NewValue as IEnumerable<object>;

            ObservableCollection<CommentResponse> k = e.NewValue as ObservableCollection<CommentResponse>;
            if(k != null)
            {
                k.CollectionChanged += (s, a) =>
                {
                    l.SetItems(l.IsOpen);
                };
            }


            if(v != null)
            {
                l.Items = new ObservableCollection<object>(v.Take(Math.Min(v.Count(), l.CompactModeItems)).Reverse());

                if(l.PART_TOGGLEBUTTON != null)
                    l.PART_TOGGLEBUTTON.Visibility = (v.Count() < l.CompactModeItems + 1) ? 
                        Visibility.Collapsed : 
                        Visibility.Visible;
            }
            else
            {
                if (l.PART_TOGGLEBUTTON != null)
                    l.PART_TOGGLEBUTTON.Visibility = Visibility.Collapsed;
            }
        }

        public static readonly DependencyProperty ItemsProperty =
            DependencyProperty.Register("Items", typeof(ObservableCollection<object>), typeof(ExpanderList), new PropertyMetadata(null));

        public ObservableCollection<object> Items
        {
            get
            {
                return (ObservableCollection<object>)this.GetValue(ItemsProperty);
            }
            set
            {
                this.SetValue(ItemsProperty, value);
            }
        }


        public static readonly DependencyProperty CompactLabelProperty =
            DependencyProperty.Register("CompactLabel", typeof(string), typeof(ExpanderList), new PropertyMetadata(string.Empty));

        public string CompactLabel
        {
            get
            {
                return (string)this.GetValue(CompactLabelProperty);
            }
            set
            {
                this.SetValue(CompactLabelProperty, value);
            }
        }


        public static readonly DependencyProperty ExpandedLabelProperty =
            DependencyProperty.Register("ExpandedLabel", typeof(string), typeof(ExpanderList), new PropertyMetadata(string.Empty));

        public string ExpandedLabel
        {
            get
            {
                return (string)this.GetValue(ExpandedLabelProperty);
            }
            set
            {
                this.SetValue(ExpandedLabelProperty, value);
            }
        }

        public static readonly DependencyProperty ItemTemplateProperty =
            DependencyProperty.Register("ItemTemplate", typeof(DataTemplate), typeof(ExpanderList), new PropertyMetadata(null));

        public DataTemplate ItemTemplate
        {
            get
            {
                return (DataTemplate)this.GetValue(ItemTemplateProperty);
            }
            set
            {
                this.SetValue(ItemTemplateProperty, value);
            }
        }

        public static readonly DependencyProperty IsOpenProperty =
            DependencyProperty.Register("IsOpen", typeof(bool), typeof(ExpanderList), 
                new PropertyMetadata(null, new PropertyChangedCallback(OnIsOpenChanged)));

        private static void OnIsOpenChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((ExpanderList)d).SetVisualState();
        }

        private void SetVisualState()
        {
            if (IsOpen)
                VisualStateManager.GoToState(this, "Opened",false); 
            else
                VisualStateManager.GoToState(this, "Normal", false);
        }

        public bool IsOpen
        {
            get
            {
                return (bool)this.GetValue(IsOpenProperty);
            }
            private set
            {
                this.SetValue(IsOpenProperty, value);
                
            }
        }

    }
}
