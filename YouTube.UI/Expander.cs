﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;

namespace YouTube.UI
{
    [TemplatePart(Name ="ExpandButton", Type =typeof(ToggleButton))]
    public class Expander: ContentControl
    {
        public Expander()
        {
            this.DefaultStyleKey = typeof(Expander);
        }

        private ToggleButton ExpandButton;
        private ContentPresenter content;

        protected override void OnApplyTemplate()
        {
            ExpandButton = GetTemplateChild("ExpandButton") as ToggleButton;
            content = GetTemplateChild("content") as ContentPresenter;

            if(ExpandButton != null && content != null)
            {
                ExpandButton.Click += (s, a) =>
                {
                    if (ExpandButton.IsChecked == true)
                        content.Height = double.NaN;
                    else
                        content.Height = MinHeight;
                };

                content.Height = MinHeight;
            }            

            base.OnApplyTemplate();
        }
    }
}
