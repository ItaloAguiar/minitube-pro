﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

// The Templated Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234235

namespace YouTube.UI
{
    [TemplateVisualState(Name = "Normal", GroupName = "CommonStates")]
    [TemplateVisualState(Name = "PointerOver", GroupName = "CommonStates")]
    [TemplateVisualState(Name = "Pressed", GroupName = "CommonStates")]
    [TemplateVisualState(Name = "Subscribed", GroupName = "SubscribeStates")]
    [TemplateVisualState(Name = "SubscribedPointerOver", GroupName = "SubscribeStates")]
    [TemplateVisualState(Name = "SubscribedPressed", GroupName = "SubscribeStates")]
    [TemplatePart(Name="Progress", Type=typeof(ProgressRing))]
    public class SubscribeButton : Control
    {
        public SubscribeButton()
        {
            this.DefaultStyleKey = typeof(SubscribeButton);

            _subscribeEventArgs = new SubscribeEventArgs();
            _subscribeEventArgs.PropertyChanged += _subscribeEventArgs_PropertyChanged;
        }
        protected ProgressRing Progress;
        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            Progress = GetTemplateChild("Progress") as ProgressRing;
            if (IsSubscribed) GoToState("Subscribed");
            this.PointerEntered += (sender, args) =>
            {
                if (IsSubscribed) GoToState("SubscribedPointerOver");
                else GoToState("PointerOver");
            };
            this.PointerExited += (sender, args) =>
            {
                if (IsSubscribed) GoToState("Subscribed");
                else GoToState("Normal");
            };
        }
        void _subscribeEventArgs_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.IsSubscribed = _subscribeEventArgs.Success;
        }
        public static readonly DependencyProperty IsSubscribedProperty =
            DependencyProperty.Register("IsSubscribed", typeof(bool), typeof(SubscribeButton), new PropertyMetadata(false, OnIsSubscribedChanged));

        private static void OnIsSubscribedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                ((SubscribeButton)d).GoToState("SubscribedPointerOver");
                ((SubscribeButton)d).GoToState("Subscribed");
            }
            else
            {
                ((SubscribeButton)d).GoToState("PointerOver");
                ((SubscribeButton)d).GoToState("Normal");
            }
            if (((SubscribeButton)d).Progress != null)
            ((SubscribeButton)d).Progress.Visibility = Visibility.Collapsed;

        }
        public bool IsSubscribed
        {
            get
            {
                return (bool)this.GetValue(IsSubscribedProperty);
            }
            set
            {
                this.SetValue(IsSubscribedProperty, value);
                if(Progress!= null)
                Progress.Visibility = Visibility.Collapsed;
            }
        }
        public static readonly DependencyProperty ChannelIDProperty =
            DependencyProperty.Register("ChannelID", typeof(string), typeof(SubscribeButton), new PropertyMetadata("", OnChannelIdChanged));

        private static void OnChannelIdChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var btn = d as SubscribeButton;
            btn._subscribeEventArgs.ChannelID = e.NewValue as string;
        }
        public string ChannelID
        {
            get
            {
                return (string)this.GetValue(ChannelIDProperty);
            }
            set
            {
                
                this.SetValue(ChannelIDProperty, value);                
            }
        }
        public static readonly DependencyProperty UserIDProperty =
            DependencyProperty.Register("UserID", typeof(string), typeof(SubscribeButton), new PropertyMetadata("", OnUserIDChanged));

        private static void OnUserIDChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var btn = d as SubscribeButton;
            btn._subscribeEventArgs.UserId = e.NewValue as string;
        }
        public string UserID
        {
            get
            {
                return (string)this.GetValue(UserIDProperty);
            }
            set
            {

                this.SetValue(UserIDProperty, value);
            }
        }
        public static readonly DependencyProperty SubscribeLabelProperty =
            DependencyProperty.Register("SubscribeLabel", typeof(string), typeof(SubscribeButton), new PropertyMetadata("Subscribe"));

        public string SubscribeLabel
        {
            get
            {
                return (string)this.GetValue(SubscribeLabelProperty);
            }
            set
            {

                this.SetValue(SubscribeLabelProperty, value);
            }
        }
        public static readonly DependencyProperty SubscriptionIdProperty =
            DependencyProperty.Register("SubscriptionId", typeof(string), typeof(SubscribeButton), new PropertyMetadata(null, OnSubscriptionIdChangend));

        private static void OnSubscriptionIdChangend(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var btn = d as SubscribeButton;
            btn._subscribeEventArgs.SubscriptionID = e.NewValue as string;
        }

        public string SubscriptionId
        {
            get
            {
                return (string)this.GetValue(SubscriptionIdProperty);
            }
            set
            {

                this.SetValue(SubscriptionIdProperty, value);
            }
        }
        public static readonly DependencyProperty SubscribedLabelProperty =
            DependencyProperty.Register("SubscribedLabel", typeof(string), typeof(SubscribeButton), new PropertyMetadata("Subscribed"));

        public string SubscribedLabel
        {
            get
            {
                return (string)this.GetValue(SubscribedLabelProperty);
            }
            set
            {

                this.SetValue(SubscribedLabelProperty, value);
            }
        }
        public static readonly DependencyProperty UnsubscribeLabelProperty =
            DependencyProperty.Register("UnsubscribeLabel", typeof(string), typeof(SubscribeButton), new PropertyMetadata("Unsubscribe"));

        public string UnsubscribeLabel
        {
            get
            {
                return (string)this.GetValue(UnsubscribeLabelProperty);
            }
            set
            {

                this.SetValue(UnsubscribeLabelProperty, value);
            }
        }
        protected override void OnPointerReleased(PointerRoutedEventArgs e)
        {
            if (RequestForSubscribe != null)
            {
                RequestForSubscribe(this, _subscribeEventArgs);
                Progress.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
        }
        protected override void OnTapped(TappedRoutedEventArgs e)
        {
            if (!IsSubscribed) GoToState("Pressed");
        }
        protected override void OnPointerMoved(PointerRoutedEventArgs e)
        {
            
            base.OnPointerMoved(e);
        }
        private void GoToState(string state)
        {
            try
            {
                VisualStateManager.GoToState(this,state, true);
            }
            catch { }
        }
        public event EventHandler<SubscribeEventArgs> RequestForSubscribe;
        private SubscribeEventArgs _subscribeEventArgs;
    }
    public class SubscribeEventArgs : EventArgs, INotifyPropertyChanged
    {
        bool success;
        public bool Success
        {
            get { return success; }
            set
            {
                success = value;

                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Success"));
            }
        }
        public string ChannelID { get; set; }
        public string SubscriptionID { get; set; }
        public string UserId { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
