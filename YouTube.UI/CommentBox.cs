﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using YouTube.API.DataModel.CommentThreads;

namespace YouTube.UI
{
    [TemplatePart(Name = "PART_RICHEDITBOX", Type = typeof(RichEditBox))]
    [TemplatePart(Name = "PART_SUBMITBUTTON", Type =typeof(Button))]
    [TemplatePart(Name = "PART_CANCELBUTTON", Type = typeof(Button))]
    public class CommentBox:Control
    {
        public CommentBox()
        {
            DefaultStyleKey = typeof(CommentBox);
        }

        protected RichEditBox PART_RICHEDITBOX;
        protected Button PART_SUBMITBUTTON;
        protected Button PART_CANCELBUTTON;


        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_RICHEDITBOX = GetTemplateChild("PART_RICHEDITBOX") as RichEditBox;
            PART_SUBMITBUTTON = GetTemplateChild("PART_SUBMITBUTTON") as Button;
            PART_CANCELBUTTON = GetTemplateChild("PART_CANCELBUTTON") as Button;

            if (PART_RICHEDITBOX != null)
            {
                PART_RICHEDITBOX.TextChanged += PART_RICHEDITBOX_TextChanged;
            }

            if(PART_CANCELBUTTON != null)
            {
                PART_CANCELBUTTON.Click += (s, a) => { IsActive = false; };
            }

            if(PART_SUBMITBUTTON != null)
            {
                PART_SUBMITBUTTON.Click += (s, a) => { Submit(); };
            }
        }

        private void PART_RICHEDITBOX_TextChanged(object sender, RoutedEventArgs e)
        {
            string text;
            PART_RICHEDITBOX.Document.GetText(Windows.UI.Text.TextGetOptions.UseCrlf, out text);
            _text = text;

            CanSubmit = !string.IsNullOrWhiteSpace(text);
        }

        public static readonly DependencyProperty UserPictureProperty =
            DependencyProperty.Register("UserPicture", typeof(ImageSource), typeof(CommentBox), null);

        public ImageSource UserPicture
        {
            get
            {
                return (ImageSource)this.GetValue(UserPictureProperty);
            }
            set
            {
                this.SetValue(UserPictureProperty, value);
            }
        }



        public static readonly DependencyProperty UserPictureWidthProperty =
            DependencyProperty.Register("UserPictureWidth", typeof(double), typeof(CommentBox), new PropertyMetadata(0));

        public double UserPictureWidth
        {
            get
            {
                return (double)this.GetValue(UserPictureWidthProperty);
            }
            set
            {
                this.SetValue(UserPictureWidthProperty, value);
            }
        }


        public static readonly DependencyProperty UserPictureHeightProperty =
            DependencyProperty.Register("UserPictureHeight", typeof(double), typeof(CommentBox), new PropertyMetadata(0));

        public double UserPictureHeight
        {
            get
            {
                return (double)this.GetValue(UserPictureHeightProperty);
            }
            set
            {
                this.SetValue(UserPictureHeightProperty, value);
            }
        }


        public static readonly DependencyProperty IsActiveProperty =
            DependencyProperty.Register("IsActive", typeof(bool), typeof(CommentBox), new PropertyMetadata(true));

        public bool IsActive
        {
            get
            {
                return (bool)this.GetValue(IsActiveProperty);
            }
            set
            {
                this.SetValue(IsActiveProperty, value);
            }
        }

        public static readonly DependencyProperty CanSubmitProperty =
            DependencyProperty.Register("CanSubmit", typeof(bool), typeof(CommentBox), new PropertyMetadata(false));

        public bool CanSubmit
        {
            get
            {
                return (bool)this.GetValue(CanSubmitProperty);
            }
            private set
            {
                this.SetValue(CanSubmitProperty, value);
            }
        }


        public static readonly DependencyProperty TopLevelCommentProperty =
            DependencyProperty.Register("TopLevelComment", typeof(object), typeof(CommentBox), new PropertyMetadata(null));

        public object TopLevelComment
        {
            get
            {
                return this.GetValue(TopLevelCommentProperty);
            }
            set
            {
                this.SetValue(TopLevelCommentProperty, value);
            }
        }


        private string _text;
        public string Text
        {
            get
            {
                return _text;
            }
            set
            {
                _text = value;
                PART_RICHEDITBOX.Document.SetText(
                    Windows.UI.Text.TextSetOptions.ApplyRtfDocumentDefaults,
                    value
                );

            }
        }        



        public static readonly DependencyProperty PlaceholderTextProperty =
            DependencyProperty.Register("PlaceholderText", typeof(string), typeof(CommentBox),
                new PropertyMetadata(null));

        public string PlaceholderText
        {
            get
            {
                return (string)this.GetValue(PlaceholderTextProperty);
            }
            set
            {
                this.SetValue(PlaceholderTextProperty, value);
            }
        }



        public static readonly DependencyProperty SubmitButtonContentProperty =
            DependencyProperty.Register("SubmitButtonContent", typeof(object), typeof(CommentBox),
                new PropertyMetadata(null));

        public object SubmitButtonContent
        {
            get
            {
                return this.GetValue(SubmitButtonContentProperty);
            }
            set
            {
                this.SetValue(SubmitButtonContentProperty, value);
            }
        }

        public static readonly DependencyProperty CancelButtonContentProperty =
            DependencyProperty.Register("CancelButtonContent", typeof(object), typeof(CommentBox),
                new PropertyMetadata(null));

        public object CancelButtonContent
        {
            get
            {
                return this.GetValue(CancelButtonContentProperty);
            }
            set
            {
                this.SetValue(CancelButtonContentProperty, value);
            }
        }

        public static readonly DependencyProperty CancelButtonVisibilityProperty =
            DependencyProperty.Register("CancelButtonVisibility", typeof(Visibility), typeof(CommentBox),
                new PropertyMetadata(Visibility.Visible));

        public Visibility CancelButtonVisibility
        {
            get
            {
                return (Visibility)this.GetValue(CancelButtonVisibilityProperty);
            }
            set
            {
                this.SetValue(CancelButtonVisibilityProperty, value);
            }
        }

        public void Clear()
        {
            Text = string.Empty;
        }

        private void Submit()
        {
            SubmitCommentEventArgs args = new SubmitCommentEventArgs();
            args.Comment = Text;
            args.TopLevelComment = TopLevelComment as Item;

            SubmitComment?.Invoke(this, args);
        }

        public event EventHandler<SubmitCommentEventArgs> SubmitComment;
    }

    public class SubmitCommentEventArgs : EventArgs
    {
        public string Comment { get; set; }
        public Item TopLevelComment { get; set; }
    }
    
}
