﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace YouTube.UI
{
    [TemplatePart(Name = "PART_IMAGE", Type =typeof(Image))]
    public class Parallax:Control
    {
        public Parallax()
        {
            DefaultStyleKey = typeof(Parallax);
        }

        protected Image PART_IMAGE;

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_IMAGE = GetTemplateChild("PART_IMAGE") as Image;
        }

        private void SetOffset(double vertical, double horizontal, double totalVertical, double totalHorizontal)
        {
            if(PART_IMAGE != null)
            {
                PART_IMAGE.Margin = new Thickness(0 ,0, Math.Min(0,-horizontal), Math.Min(0, -vertical));
            }
        }

        public static readonly DependencyProperty ImageSourceProperty =
            DependencyProperty.Register("ImageSource", typeof(ImageSource), typeof(Parallax), null);

        public ImageSource ImageSource
        {
            get
            {
                return (ImageSource)this.GetValue(ImageSourceProperty);
            }
            set
            {
                this.SetValue(ImageSourceProperty, value);
            }
        }


        public static readonly DependencyProperty VerticalOffsetEndProperty =
            DependencyProperty.Register("VerticalOffsetEnd", typeof(double), typeof(Parallax), null);

        public double VerticalOffsetEnd
        {
            get
            {
                return (double)this.GetValue(VerticalOffsetEndProperty);
            }
            set
            {
                this.SetValue(VerticalOffsetEndProperty, value);
            }
        }


        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(ScrollViewer), typeof(Parallax), 
                new PropertyMetadata(null, new PropertyChangedCallback(OnSourceChanded)));

        private static void OnSourceChanded(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Parallax p = d as Parallax;
            ScrollViewer sv = e.NewValue as ScrollViewer;
            if(sv != null)
            {
                sv.ViewChanged += (s, a) =>
                {
                    p.SetOffset(sv.VerticalOffset, sv.HorizontalOffset, sv.ScrollableHeight, sv.ScrollableWidth);
                };
            }
        }

        public ScrollViewer Source
        {
            get
            {
                return (ScrollViewer)this.GetValue(SourceProperty);
            }
            set
            {
                this.SetValue(SourceProperty, value);
            }
        }
    }
}
